/*
 * GOOD! BUILD THIS ONE!
 */

import java_cup.runtime.*;

%%

%public
%class Scanner
%implements sym

%unicode

%line
%column
%char

%cup
%cupdebug

%{
  private int comment_count = 0;

  StringBuffer string = new StringBuffer();
  
  private Symbol symbol(int type) {
    return new QuackSymbol(type, yyline+1, yycolumn+1);
  }

  private Symbol symbol(int type, Object value) {
    return new QuackSymbol(type, yyline+1, yycolumn+1, value);
  }

  /** 
   * assumes correct representation of a long value for 
   * specified radix in scanner buffer from <code>start</code> 
   * to <code>end</code> 
   */
  private long parseLong(int start, int end, int radix) {
    long result = 0;
    long digit;

    for (int i = start; i < end; i++) {
      digit  = Character.digit(yycharat(i),radix);
      result*= radix;
      result+= digit;
    }

    return result;
  }
%}

/* main character classes */

ALPHA=[A-Za-z]
DIGIT=[0-9]
NONNEWLINE_WHITE_SPACE_CHAR=[\ \t]
NEWLINE=\r|\n|\r\n
WHITE_SPACE_CHAR=[\n\r\ \t]
STRING_TEXT=((\\\")|(\\0)|(\\b)|(\\t)|(\\n)|(\\r)|(\\f)|(\\\\)|[^\n\r\"\\])*
COMMENT_TEXT=([^*/\n]|[^*\n]"/"[^*\n]|[^/\n]"*"[^/\n]|"*"[^/\n]|"*"[\r\n]|"/"[^*\n])*
MULTILINE_STRING_TEXT=(\"[^\"]|\"\"[^\"]|[^\"])*

%state COMMENT

%%

<YYINITIAL> {

  /* Match single line comments */
  "//".* { }

  /* Match keywords */
  "class" { return ( symbol(sym.CLASS)); }
  "def" { return ( symbol(sym.DEF)); }
  "extends" { return ( symbol(sym.EXTENDS)); }
  "if" { return ( symbol(sym.IF)); }
  "elif" { return ( symbol(sym.ELIF)); }
  "else" { return ( symbol(sym.ELSE)); }
  "while" { return ( symbol(sym.WHILE)); }
  "return" { return ( symbol(sym.RETURN)); }
  
  /* Don't think I need to handle predefined identifiers, but if I do it goes here */

  /* Match punctuation */
  "+" { return (symbol(PLUS)); }
  "-" { return (symbol(MINUS)); }
  "*" { return (symbol(TIMES)); }
  "/" { return (symbol(DIVIDE)); }
  "==" { return (symbol(EQUALS)); }
  "<=" { return (symbol(ATMOST)); }
  ">=" { return (symbol(ATLEAST)); }
  "<" { return (symbol(LESS)); }
  ">" { return (symbol(MORE)); }
  "and" { return (symbol(AND)); }
  "or" { return (symbol(OR)); }
  "not" { return (symbol(NOT)); }
  "{" { return (symbol(LBRACE)); }
  "}" { return (symbol(RBRACE)); }
  "=" { return (symbol(GETS)); }
  "(" { return (symbol(LPAREN)); }
  ")" { return (symbol(RPAREN)); }
  "," { return (symbol(COMMA)); }
  ";" { return (symbol(SEMICOLON)); }
  "." { return (symbol(DOT)); }
  ":" { return (symbol(COLON)); }

  /* Match identifiers */
  [a-zA-Z_][a-zA-Z0-9_]* { return symbol(IDENT, new String(yytext())); }

  /* Match integer literals */
  [0-9]+ { return (symbol(INT, Integer.parseInt(yytext()))); }

  {NONNEWLINE_WHITE_SPACE_CHAR}+ { }

  "/*" { yybegin(COMMENT); comment_count++; }

  
  "\"\"\""{MULTILINE_STRING_TEXT}"\"\"\"" {
    String str = yytext().substring(3, yytext().length()-3);
    return (symbol(STRING, new String(yytext())));
  }

  \"{STRING_TEXT}\" {
    String str =  yytext().substring(0,yytext().length());
    return (symbol(STRING, new String(yytext())));
  }
  
  \"{STRING_TEXT}(\r\n|[\r\n]) {
    String str =  yytext().substring(0,yytext().length());
    Utility.error(Utility.E_UNCLOSEDSTR, yyline, yytext());
    return (symbol(STRING, new String(yytext())));
  }
}

<COMMENT> {
  "/*" { comment_count++; }
  "*/" { if (--comment_count == 0) yybegin(YYINITIAL); }
  {COMMENT_TEXT} { }
}


{NEWLINE} { }

. {
    Utility.error(Utility.E_UNMATCHED, yyline, yytext());
}


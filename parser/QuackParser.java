import java.io.*;

// TODO: Make this read from stdin when given no arguments

/**
 * Simple test driver for the java parser. Just runs it on some
 * input files, gives no useful output.
 */
public class QuackParser {

  public static void main(String argv[]) throws Exception {

    for (int i = 0; i < argv.length; i++) {
      try {
        Scanner s = new Scanner(new FileReader(argv[i]));
        parser p = new parser(s);
        p.parse();
        
        System.out.println("Finished parse with no errors");
      }
      catch (java.lang.RuntimeException e) {
        System.exit(1);
      }
      catch (FileNotFoundException e)
      {
        System.err.println("File could not be found");
        System.exit(1);
      }
    }
  }
}

#!/usr/bin/env python3
#
# Jeremy Sigrist
# This program generates a driver script for the java parser.
# Using a script to generate a script ensures that the paths are always accurate 
# and machine independent.

import os
from sys import argv

def write_driver(base_path, stream):
    print('#!/bin/bash', file = stream)
    print('', file = stream)
    print('filename=$(basename "$1")', file = stream)
    print('output_filename="${filename%.*}"', file = stream)
    print('', file = stream)
    print('if [ "$#" -ne 1 ]; then', file = stream)
    print('    >&2 echo "Usage: quack [input filename]"', file = stream)
    print('else', file = stream)
    print('    java -jar "{}/quack_compiler.jar" "$@" "$output_filename"'.format(os.getcwd()), file=stream)
    print('fi', file = stream)


if __name__ == '__main__':

    # Make sure we have enough command line arguments
    if len(argv) != 2:
        print('Usage: ./driver_gen [filename]')
        exit(1)

    with open(argv[1], mode='w') as input_file:
        write_driver(os.getcwd(), input_file)

    os.chmod(argv[1], 0o755)


package compiler;

import java.util.List;

import ast.Identifier;

public interface QuackAcceptor {
	
	// Use illegal characters in the type name for NO_TYPE
	// so no one can make a type with that name.
	public static Identifier NO_TYPE = new Identifier("$@NO_TYPE$@");

	public void accept(QuackVisitor v);
	
	public List<QuackAcceptor> getChildren();
	
}

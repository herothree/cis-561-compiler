package compiler;

public interface QuackVisitor
{
    /**
     * Called before the visit method on each node to visit
     * @param n The object to visit
     * @return true if the object should be visited, false otherwise
     */
	public boolean preVisit(QuackAcceptor n);
    
	/**
     * Called to perform an action on each node
     * @param n The object to visit
     */	
	public void visit(QuackAcceptor n);

	/**
     * Called after the visit method on each node
     * @param n The object to visit
     */
	public void postVisit(QuackAcceptor n);

}

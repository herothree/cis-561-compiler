package compiler;

import java.io.*;
import java.nio.charset.StandardCharsets;

import ast.*;
import codegen.CodeGenerator;
import java_cup.Main;
import java_cup.runtime.Symbol;
import parser.*;

/**
 * Simple test driver for the quack parser. Just runs it on some input files,
 * gives no useful output.
 */
public class QuackCompiler
{

    // Should we print anything outside of this class?
    public static boolean PRINT_INFO = true;

    // Higher levels -> more information
    public static int DEBUG = 0;

    public static void main(String argv[]) throws Exception
    {

        if (2 != argv.length || argv[0].lastIndexOf('.') == -1)
        {
            System.err.println("Usage: ./quack [input filename] [output filename]");
            System.exit(-1);
        }

        try
        {
            parseFile(new FileReader(argv[0]), argv[1]);
        }
        catch (FileNotFoundException e)
        {
            System.err.printf("Error opening %s\n", e.getMessage());
            System.exit(1);
        }
        catch (Exception e)
        {
            System.err.println(e.getMessage());
            System.exit(1);
        }
    }

    public static void parseFile(Reader contents, String outputFilename) throws Exception
    {
        Scanner s = new Scanner(contents);

        try
        {
            parser p = new parser(s);
            Symbol symbol = p.parse();
            Program program = (Program) symbol.value;

            TypeChecker typeChecker = new TypeChecker(program);

            // Make sure the summary prints after the typechecker has finished
            // printing
            // error messages.
            Thread.sleep(10);
            if (!typeChecker.typeCheck())
            {
                System.err.println("Type check failed");
                System.exit(-1);
            }
            
            // Java Paths class seems to need a path that begins with / or ./ 
            // (so just "robot.qk") will break it.
            if (!outputFilename.startsWith("/") && !outputFilename.startsWith("./"))
            {
                outputFilename = "./" + outputFilename;
            }

            CodeGenerator codeGenerator = new CodeGenerator(typeChecker, program, outputFilename);
            String line;
            InputStream in = (Main.class.getResourceAsStream("/preamble_c.txt"));
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(in, StandardCharsets.UTF_8));

            while ((line = br.readLine()) != null)
            {
                codeGenerator.getPreamble_c().add(line);
            }
            br.close();

            in = (Main.class.getResourceAsStream("/preamble_h.txt"));
            br = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
            while ((line = br.readLine()) != null)
            {
                codeGenerator.getPreamble_h().add(line);
            }
            br.close();

            codeGenerator.generate();
        }
        catch (RuntimeException ex)
        {
            System.err.println("error:" + ex.getMessage());
            ex.printStackTrace();
            System.exit(-1);
        }
    }
}

package compiler;

public class UniqueID
{
	private static int current = 0;
	
	/**
	 * @return An integer one greater than the last time it was called
	 */
	public static int getNext()
	{
		// Note that 1 is returned as the first unique ID
		return ++current;
	}
}

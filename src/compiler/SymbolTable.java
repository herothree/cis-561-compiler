package compiler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import ast.FormalArg;
import ast.Identifier;
import codegen.MethodTable;
import errors.DuplicateVariableNameException;

/**
 * Symbol table jobs
 * 	Keep all declared variables in one place
 *  Keep track of variable types
 *  Keep track of variable scopes
 *  Keep track of when variables are declared so they aren't read before they are written
 */
public class SymbolTable
{
	
	protected HashMap<String, SymbolInformation> table;
	protected MethodTable methodTable;
	protected MethodTable memberVarTable;
	
	public SymbolTable()
	{
		table = new HashMap<String, SymbolInformation>();
		methodTable = new MethodTable();
		memberVarTable = new MethodTable();
		
		// Add in known methods such as ADD, MUL, etc
		prepopulate();
	}
	
	private void prepopulate() {
	    // THE ORDER HERE MUST MATCH THE ORDER IN PREAMBLE_C.TXT
		
		// Prepopulate the methods for Obj
		addSymbol(0, new Identifier("Obj"), new MethodInformation(new Identifier("Obj")));
        methodTable.addClass(new Identifier("Obj"), null);
        memberVarTable.addClass(new Identifier("Obj"), null);
		
		addSymbol(new Identifier("PRINT"), new Identifier("Obj"), new MethodInformation(new Identifier("Nothing"),
				new ArrayList<Identifier>(), new ArrayList<Identifier>()));
		methodTable.addMethod(new Identifier("Obj"), new Identifier("Obj"), new Identifier("STR"));
		addSymbol(new Identifier("STR"), new Identifier("Obj"), new MethodInformation(new Identifier("String"),
				new ArrayList<Identifier>(), new ArrayList<Identifier>()));
	    methodTable.addMethod(new Identifier("Obj"), new Identifier("Obj"), new Identifier("PRINT"));
        addSymbol(new Identifier("EQUALS"), new Identifier("Obj"), new MethodInformation(new Identifier("Boolean"), 
                Arrays.asList(new Identifier[]{new Identifier("Obj")}), Arrays.asList(new Identifier[]{new Identifier("other")})));
        methodTable.addMethod(new Identifier("Obj"), new Identifier("Obj"), new Identifier("EQUALS"));

        
		// Prepopulate the methods for Int
		addSymbol(0, new Identifier("Int"), new MethodInformation(new Identifier("Int")));
		
		// caller, return type, arg_types
		addSymbol(new Identifier("STR"), new Identifier("Int"), new MethodInformation(new Identifier("String"), 
                new ArrayList<Identifier>(),new ArrayList<Identifier>()));
		addSymbol(new Identifier("EQUALS"), new Identifier("Int"), new MethodInformation(new Identifier("Boolean"), 
                Arrays.asList(new Identifier[]{new Identifier("Int")}),Arrays.asList(new Identifier[]{new Identifier("Other")})));

		addSymbol(new Identifier("PLUS"), new Identifier("Int"), new MethodInformation(new Identifier("Int"), 
				Arrays.asList(new Identifier[]{new Identifier("Int")}),Arrays.asList(new Identifier[]{new Identifier("Other")})));
		addSymbol(new Identifier("MINUS"), new Identifier("Int"), new MethodInformation(new Identifier("Int"), 
				Arrays.asList(new Identifier[]{new Identifier("Int")}), Arrays.asList(new Identifier[]{new Identifier("Other")})));
		addSymbol(new Identifier("TIMES"), new Identifier("Int"), new MethodInformation(new Identifier("Int"), 
				Arrays.asList(new Identifier[]{new Identifier("Int")}), Arrays.asList(new Identifier[]{new Identifier("Other")})));
		addSymbol(new Identifier("DIVIDE"), new Identifier("Int"), new MethodInformation(new Identifier("Int"), 
				Arrays.asList(new Identifier[]{new Identifier("Int")}), Arrays.asList(new Identifier[]{new Identifier("Other")})));
		addSymbol(new Identifier("ATMOST"), new Identifier("Int"), new MethodInformation(new Identifier("Boolean"), 
				Arrays.asList(new Identifier[]{new Identifier("Int")}), Arrays.asList(new Identifier[]{new Identifier("Other")})));
		addSymbol(new Identifier("ATLEAST"), new Identifier("Int"), new MethodInformation(new Identifier("Boolean"), 
				Arrays.asList(new Identifier[]{new Identifier("Int")}), Arrays.asList(new Identifier[]{new Identifier("Other")})));
		addSymbol(new Identifier("LESS"), new Identifier("Int"), new MethodInformation(new Identifier("Boolean"), 
				Arrays.asList(new Identifier[]{new Identifier("Int")}), Arrays.asList(new Identifier[]{new Identifier("Other")})));
		addSymbol(new Identifier("MORE"), new Identifier("Int"), new MethodInformation(new Identifier("Boolean"), 
				Arrays.asList(new Identifier[]{new Identifier("Int")}), Arrays.asList(new Identifier[]{new Identifier("Other")})));
		
		methodTable.addClass(new Identifier("Int"), new Identifier("Obj"));
        methodTable.addMethod(new Identifier("Int"), new Identifier("Int"), new Identifier("STR"));
        methodTable.addMethod(new Identifier("Int"), new Identifier("Obj"), new Identifier("PRINT"));
        methodTable.addMethod(new Identifier("Int"), new Identifier("Int"), new Identifier("EQUALS"));
        methodTable.addMethod(new Identifier("Int"), new Identifier("Int"), new Identifier("PLUS"));
        methodTable.addMethod(new Identifier("Int"), new Identifier("Int"), new Identifier("MINUS"));
        methodTable.addMethod(new Identifier("Int"), new Identifier("Int"), new Identifier("TIMES"));
        methodTable.addMethod(new Identifier("Int"), new Identifier("Int"), new Identifier("DIVIDE"));
        methodTable.addMethod(new Identifier("Int"), new Identifier("Int"), new Identifier("ATMOST"));
        methodTable.addMethod(new Identifier("Int"), new Identifier("Int"), new Identifier("ATLEAST"));
        methodTable.addMethod(new Identifier("Int"), new Identifier("Int"), new Identifier("LESS"));
        methodTable.addMethod(new Identifier("Int"), new Identifier("Int"), new Identifier("MORE"));

        // Add member variables
        memberVarTable.addClass(new Identifier("Int"), new Identifier("Obj"));
		memberVarTable.addMethod(new Identifier("Int"), new Identifier("Int"), new Identifier("value"));
		
		// Prepopulate methods for String
		addSymbol(0, new Identifier("String"), new MethodInformation(new Identifier("String")));
		methodTable.addClass(new Identifier("String"), new Identifier("Obj"));
	    addSymbol(new Identifier("STR"), new Identifier("String"), new MethodInformation(new Identifier("String"),
	                new ArrayList<Identifier>(), new ArrayList<Identifier>()));
	    methodTable.addMethod(new Identifier("String"), new Identifier("String"), new Identifier("STR"));
		addSymbol(new Identifier("PRINT"), new Identifier("String"), new MethodInformation(new Identifier("Nothing"),
                new ArrayList<Identifier>(), new ArrayList<Identifier>()));
        methodTable.addMethod(new Identifier("String"), new Identifier("String"), new Identifier("PRINT"));
        addSymbol(new Identifier("EQUALS"), new Identifier("String"), new MethodInformation(new Identifier("Boolean"),
                Arrays.asList(new Identifier[]{new Identifier("String")}), Arrays.asList(new Identifier[]{new Identifier("Other")})));
        methodTable.addMethod(new Identifier("String"), new Identifier("String"), new Identifier("EQUALS"));		
        addSymbol(new Identifier("PLUS"), new Identifier("String"), new MethodInformation(new Identifier("String"), 
				Arrays.asList(new Identifier[]{new Identifier("String")}), Arrays.asList(new Identifier[]{new Identifier("Other")})));
        methodTable.addMethod(new Identifier("String"), new Identifier("String"), new Identifier("PLUS"));
		addSymbol(new Identifier("LESS"), new Identifier("String"), new MethodInformation(new Identifier("Boolean"), 
                Arrays.asList(new Identifier[]{new Identifier("String")}), Arrays.asList(new Identifier[]{new Identifier("Other")})));
        methodTable.addMethod(new Identifier("String"), new Identifier("String"), new Identifier("LESS"));
        addSymbol(new Identifier("MORE"), new Identifier("String"), new MethodInformation(new Identifier("Boolean"), 
                Arrays.asList(new Identifier[]{new Identifier("String")}), Arrays.asList(new Identifier[]{new Identifier("Other")})));
        methodTable.addMethod(new Identifier("String"), new Identifier("String"), new Identifier("MORE"));
        addSymbol(new Identifier("ATMOST"), new Identifier("String"), new MethodInformation(new Identifier("Boolean"), 
                Arrays.asList(new Identifier[]{new Identifier("String")}), Arrays.asList(new Identifier[]{new Identifier("Other")})));
        methodTable.addMethod(new Identifier("String"), new Identifier("String"), new Identifier("ATMOST"));
        addSymbol(new Identifier("ATLEAST"), new Identifier("String"), new MethodInformation(new Identifier("Boolean"), 
                Arrays.asList(new Identifier[]{new Identifier("String")}), Arrays.asList(new Identifier[]{new Identifier("Other")})));
        methodTable.addMethod(new Identifier("String"), new Identifier("String"), new Identifier("ATLEAST"));
        
        // Add member variables
        memberVarTable.addClass(new Identifier("String"), new Identifier("Obj"));
        memberVarTable.addMethod(new Identifier("String"), new Identifier("String"), new Identifier("text"));

        
		// Prepopulate methods for Boolean
		addSymbol(0, new Identifier("Boolean"), new MethodInformation(new Identifier("Boolean")));
		methodTable.addClass(new Identifier("Boolean"), new Identifier("Obj"));
		addSymbol(new Identifier("AND"), new Identifier("Boolean"), new MethodInformation(new Identifier("Boolean"), 
				Arrays.asList(new Identifier[]{new Identifier("Boolean")}), Arrays.asList(new Identifier[]{new Identifier("Other")})));
        methodTable.addMethod(new Identifier("Boolean"), new Identifier("Boolean"), new Identifier("STR"));
		addSymbol(new Identifier("OR"), new Identifier("Boolean"), new MethodInformation(new Identifier("Boolean"), 
				Arrays.asList(new Identifier[]{new Identifier("Boolean")}), Arrays.asList(new Identifier[]{new Identifier("Other")})));
	    
		memberVarTable.addClass(new Identifier("Boolean"), new Identifier("Obj"));
		memberVarTable.addMethod(new Identifier("Boolean"), new Identifier("Boolean"), new Identifier("value"));

	}

	public void addSymbol(int scope, Identifier var, SymbolInformation si)
	{
	    if (var.getData().equals("Integer"))
	    {
	           System.out.println("FOUND ITTT!!!!!!");
	    }
		table.put(combineNames(scope, var), si);
	}
	
	public void addSymbol(Identifier var, Identifier className, SymbolInformation si) throws DuplicateVariableNameException
	{
	    if (var.getData().equals("Integer") || className.getData().equals("Integer"))
	    {
	        System.out.println("FOUND IT!!!!!!");
	    }
		// If adding a class variable, combine them the class name so 
		// they are associated with the correct class. combineNames 
		// includes an illegal character as well so the new name 
		// can't clash with a different variable name.
		table.put(combineNames(0, var, className), si);
	}
	
	private String combineNames(int scope, Identifier var)
	{
		if (0 == scope)
		{
			return var.getData();
		}
		else
		{
			return Integer.toString(scope) + "$" + var.getData();
		}
	}
	
	private String combineNames(int scope, Identifier var, Identifier className)
	{
		// For global scope don't bother putting the 0 on.
		if (0 == scope)
		{
			// $ is an illegal character 
			return className.getData() + "$" + var.getData();
		}
		else
		{
			return Integer.toString(scope) + "$" + combineNames(0, var, className);
		}
	}
	
	public String extractName(String str)
	{
		return str.substring(str.lastIndexOf("$") + 1);
	}
	
	public boolean contains(int scope,Identifier id)
	{
		return this.table.containsKey(combineNames(scope, id));
	}
	
	public boolean contains(Identifier var, Identifier className)
	{
		return this.table.containsKey(combineNames(0, var, className));
	}
	
	public SymbolInformation get(int scope, Identifier id)
	{
		return this.table.get(combineNames(scope, id));
	}
	
	public SymbolInformation get(Identifier var, Identifier className)
	{
		// Classes are always global scope, so just send 0 automatically.
		return this.table.get(combineNames(0, var, className));
	}
	
	public boolean isMethod(Identifier var, Identifier className)
	{
		
		if (this.contains(var, className) && this.get(var, className) instanceof MethodInformation)
		{
			return true;			
		}
		
		return false;
	}
	
	public boolean isMethod(int scope, Identifier className)
	{
		
		if (this.contains(scope, className) && this.get(scope, className) instanceof MethodInformation)
		{
			return true;			
		}
		
		return false;
	}
	
	public MethodInformation getMethod(Identifier var, Identifier className)
	{
		if (!isMethod(var, className))
		{
			return null;
		}
		
		return ((MethodInformation) this.get(var, className));
	}
	
	public Set<Identifier> getAllMemberMethods(Identifier className)
	{
		// Shouldn't have to worry about scopes since the only variables
		// we care about here won't have local scope.
		int endIndex;
		Set<Identifier> result = new HashSet<Identifier>();
		for (String name : this.table.keySet())
		{
		    // extract the part of the variable name up to the $ 
		    // to get the class name.
	        endIndex = name.indexOf("$");
	        if (-1 == endIndex)
	        {
	            endIndex = name.length();
	        }
	            
			if (name.substring(0, endIndex).equals(className.getData())
					&& this.isMethod(new Identifier(extractName(name)), className))
			{
				result.add(new Identifier(extractName(name)));
			}
		}
		
		return result;
	}
	
	public Set<FormalArg> getAllMemberVariables(Identifier className)
	{
	    int endIndex;
		Set<FormalArg> result = new HashSet<FormalArg>();
		for (String name : this.table.keySet())
		{
		    //System.out.println(name);
		    endIndex = name.indexOf("$");
		    if (-1 == endIndex)
		    {
		        endIndex = name.length();
		    }
		    String tmp = name.substring(0, endIndex);
			if (tmp.equals(className.getData())
					&& !this.isMethod(new Identifier(extractName(name)), className))
			{
				result.add(new FormalArg(extractName(name), table.get(name).type.getData(), null));
			}
		}
		
		// The constructor will sneak in here because it's not technically 
		// a method. Remove it.
		result.remove(new FormalArg(className.getData(), className.getData(), null));
		
		return result;
	}
	
	public Set<FormalArg> getAllScopeVariables(int scope)
	{
	    Set<FormalArg> result = new HashSet<FormalArg>();
	    int endIndex;
	    for (String name : this.table.keySet())
	    {
	        endIndex = name.indexOf("$");
            if (-1 == endIndex)
            {
                endIndex = name.length();
            }
            
	        if (name.substring(0, endIndex).equals(Integer.toString(scope)))
	        {
	            result.add(new FormalArg(extractName(name), table.get(name).getType().getData(), null));
	        }
	    }
	    
	    return result;
	}
	
	public MethodInformation getConstructor(Identifier className)
	{
	    return getMethod(0, className);
	}
	
	public MethodInformation getMethod(int scope, Identifier className)
	{
		if (!isMethod(scope, className))
		{
			return null;
		}
		
		return ((MethodInformation) this.get(scope, className));
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		
		for (String key : table.keySet())
		{
			sb.append(key + " : ");
			sb.append(table.get(key).toString());
			sb.append("\n");
		}
		
		return sb.toString();
	}

    public MethodTable getMethodTable()
    {
        return methodTable;
    }

    public void setMethodTable(MethodTable methodTable)
    {
        this.methodTable = methodTable;
    }

    public MethodTable getMemberVarTable()
    {
        return memberVarTable;
    }

    public void setMemberVarTable(MethodTable memberVarTable)
    {
        this.memberVarTable = memberVarTable;
    }
}

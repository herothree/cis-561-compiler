package compiler;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;

import ast.Identifier;
import errors.DuplicateVariableNameException;
import errors.TypeErrorException;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class ClassHierarchyTree 
{
	// The root node for the tree
	private ClassHierarchyNode root;
	
	// Any class that does not yet have a valid superclass
	private List<ClassHierarchyNode> orphans;
	
	private int size;
	
	public ClassHierarchyTree()
	{
		orphans = new ArrayList<ClassHierarchyNode>();
		
		// Given initial class hierarchy
		root = new ClassHierarchyNode(new Identifier("Obj"), null);
		root.addChild(new Identifier("String"), root);
		root.addChild(new Identifier("Int"), root);
		root.addChild(new Identifier("Boolean"), root);
		root.addChild(new Identifier("Nothing"), null);
		size = 4;
	}
	
	public List<Identifier> getAllClassNames()
	{
		List<Identifier> result = new ArrayList<Identifier>();
		ClassHierarchyNode current = null;
		Queue<ClassHierarchyNode> queue = new ArrayDeque<ClassHierarchyNode>();
		queue.add(root);

		
		// Perform a breadth first traversal of the tree to
		// add all nodes to the list.
		while (queue.size() > 0)
		{
			current = queue.poll();
			
			result.add(current.getName());
			
			queue.addAll(current.getChildren());
		}
		
		return result;
	}
	
	/**
	 * @return The size of the tree (not including any class nodes that are currently orphans).
	 */
	public int getSize()
	{
		return size;
	}
	
	public void addClass(Identifier name, Identifier parentName)
	{
		ClassHierarchyNode parentNode = findNode(parentName);
		if (null == parentNode)
		{
			this.orphans.add(new ClassHierarchyNode(name, new ClassHierarchyNode(parentName)));
		}
		else
		{
			addClass(new ClassHierarchyNode(name, parentNode));			
		}
	}
	
	public void addClass(ClassHierarchyNode chn)
	{
		boolean added = addClassHelper(chn);
		
		if (added)
		{
			++size;
			
			// If we added a node, try and add all other orphans because they might have
			// a valid parent class now.
		    for (Iterator<ClassHierarchyNode> it = orphans.iterator(); it.hasNext(); )
		    {
		        if (addClassHelper(it.next()))
		        {
		            it.remove();
		        }
		    }
		}
		else
		{
			// Shouldn't get here
			orphans.add(chn);			
		}
	}
	
	public boolean addClassHelper(Identifier name, ClassHierarchyNode parent)
	{
		return addClassHelper(new ClassHierarchyNode(name, parent));
	}
	
	public boolean addClassHelper(ClassHierarchyNode chn)
	{
		boolean added = false;
		ClassHierarchyNode current = null;
		Queue<ClassHierarchyNode> queue = new ArrayDeque<ClassHierarchyNode>();
		queue.add(root);
		
		// How many times have we seen a node with the same name as the one to add?
		// Should be one because the node will match itself once added
		int matchCount = 0;
		
		// Perform a breadth first traversal of the tree to find
		// where to add the node and make sure we have no duplicate names
		while (queue.size() > 0)
		{
			current = queue.poll();
			
			if (current.getName().equals(chn.getName()))
			{
				matchCount += 1;
				
				if (matchCount > 1)
				{
					throw new DuplicateVariableNameException(String.format("Duplicate class name: %s", current.getName().toString()), null);
				}
			}
			
			if (current.getName().equals(chn.getParentName()))
			{
				current.addChild(chn);
				added = true;
			}

			queue.addAll(current.getChildren());
		}
		

		
		return added;
	}
	
	public boolean containsClass(Identifier name)
	{
		return null != findNode(name);
	}
	
	/**
	 * Finds a node in the class hierarchy tree
	 * @param name The name of the node to search for
	 * @return null if the node cannot be found, otherwise the node
	 */
	private ClassHierarchyNode findNode(Identifier name)
	{
		Queue<ClassHierarchyNode> queue = new ArrayDeque<ClassHierarchyNode>();
		queue.add(root);
		while (queue.size() > 0)
		{
			if (queue.peek().getName().equals(name))
			{
				return queue.poll();
			}
			else
			{
				queue.addAll(queue.poll().getChildren());
			}
		}
		
		return null;
	}
	
	public boolean isValid()
	{
		// The hierarchy is valid as long as there are no nodes that have no parent
		return orphans.isEmpty();
	}
	
	/**
	 * 
	 * @return A list of the classes that do not have parents in the class hierarchy.
	 */
	public List<ClassHierarchyNode> getOrphans()
	{
		return orphans;
	}
	
	/**
	 * Checks to see if child is a descendant of ancestor in the class hierarchy tree
	 * @param child The child to check
	 * @param ancestor The ancestor to check
	 * @return True if child is a descendant of ancestor (or they are the same), false otherwise
	 */
	public boolean isSubclass(Identifier child, Identifier ancestor)
	{
		ClassHierarchyNode node = findNode(child);
		
		while (null != node)
		{
			if (node.getName().equals(ancestor))
			{
				return true;
			}
			
			node = node.getParent();
		}
		
		return false;
	}
	
	public boolean isDirectParent(Identifier parent, Identifier child)
	{
		ClassHierarchyNode chn = findNode(child);
		return chn.getParent() != null && chn.getParent().getName().equals(parent);
	}
	
	/**
	 * Checks to see if a is a subclass (or the same class) as b.
	 * Throws a TypeErrorException if this is not true.
	 * @param a
	 * @param b
	 */
	public void checkSubclass(Identifier a, Identifier b, Location loc)
	{
		
		if (!this.isSubclass(a, b))
		{
			if (TypeChecker.UNBOUND.equals(a) && TypeChecker.UNBOUND.equals(b))
			{
				throw new TypeErrorException(String.format("Type error: %s is not a subclass of %s.", "Unknown type", "Unknown type"), loc);				
			}
			else if (TypeChecker.UNBOUND.equals(a))
			{
				throw new TypeErrorException(String.format("Type error: %s is not a subclass of %s.", "Unknown type", b.getData()), loc);				
			}
			else if (TypeChecker.UNBOUND.equals(b))
			{
				throw new TypeErrorException(String.format("Type error: %s is not a subclass of %s.", a.getData(), "Unknown type"), loc);
			}
			else
			{
				throw new TypeErrorException(String.format("Type error: %s is not a subclass of %s.", a.getData(), b.getData()), loc);
			}
		}
	}
	
	/**
	 * Finds the parent of a class (null if no parent or child not in tree).
	 * @param child The class who's parent needs to be found
	 * @return The name of the parent class, or null.
	 */
	public Identifier getParent(Identifier child)
	{
		ClassHierarchyNode chn = findNode(child);
		if (null != chn && null != chn.getParent())
		{
			return chn.getParent().getName();
		}
		
		return null;
	}
	
	public void checkType(Identifier toCheck, Location loc)
	{
		if (!containsClass(toCheck))
		{
			throw new TypeErrorException(String.format("%s is not a known class or type", toCheck.getData()), loc);
		}
	}
	
	public Identifier findLowestCommonAncestor(Identifier a, Identifier b)
	{
	    if (!containsClass(a))
	    {
	        throw new TypeErrorException(String.format("No such class: %s", a.getData()), a.getLocation());
	    }
	    
	    if (!containsClass(b))
	    {
	        throw new TypeErrorException(String.format("No such class: %s", b.getData()), b.getLocation());
	    }
	    
	    // Start with a, walk up tree until we find one that is a subclass
	    
	    Identifier current = new Identifier(a.getData());
	    
	    while (root.getName() != current)
	    {
	        if (isSubclass(b, current))
	        {
	            return current;
	        }
	        
	        current = getParent(current);
	    }
	    
	    return current;
	}
	
	public static void main(String[] args)
	{
		ClassHierarchyTree tree = new ClassHierarchyTree();
		tree.addClass(new Identifier("a"), new Identifier("b"));
		tree.addClass(new Identifier("b"), new Identifier("String"));
		tree.addClass(new Identifier("c"), new Identifier("Boolean"));

		System.out.println("Valid: " + tree.isValid());
		System.out.println(tree.containsClass(new Identifier("a")));
	}

	public ClassHierarchyNode getRoot() {
		return root;
	}

	public void setRoot(ClassHierarchyNode root) {
		this.root = root;
	}
}

class ClassHierarchyNode
{
	private List<ClassHierarchyNode> children;
	private Identifier name;
	private ClassHierarchyNode parent;
	
	public ClassHierarchyNode(Identifier name, ClassHierarchyNode parent, ClassHierarchyNode chn)
	{
		this(name, parent);
		children.add(chn);
	}
	
	public ClassHierarchyNode(Identifier name, ClassHierarchyNode parent, ClassHierarchyNode chn1, ClassHierarchyNode chn2)
	{
		this(name, parent);
		children.add(chn1);
		children.add(chn2);
	}
	
	public ClassHierarchyNode(Identifier name, ClassHierarchyNode parent, Collection<ClassHierarchyNode> children)
	{
		this(name, parent);
		children.addAll(children);
	}
	
	public ClassHierarchyNode(Identifier name, ClassHierarchyNode parent)
	{
		children = new ArrayList<ClassHierarchyNode>();
		this.name = name;
		this.parent = parent;
	}
	
	public ClassHierarchyNode(Identifier name)
	{
		this(name, null);
	}
	
	public Identifier getName()
	{
		return this.name;
	}
	
	public Identifier getParentName()
	{
		return this.parent.name;
	}
	
	public ClassHierarchyNode getParent()
	{
		return this.parent;
	}
	
	public List<ClassHierarchyNode> getChildren()
	{
		return this.children;
	}
	
	public void addChild(ClassHierarchyNode chn)
	{
		chn.parent = this;
		children.add(chn);
	}
	
	public void addChild(Identifier name, ClassHierarchyNode parent)
	{
		this.addChild(new ClassHierarchyNode(name, parent));
	}
}

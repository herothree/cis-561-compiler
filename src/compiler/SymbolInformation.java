package compiler;

import ast.Identifier;

/**
 * This class is designed to be stored in a hash table as the value,
 * keyed by an identifier that is the name of the symbol in question.
 * 
 * Currently it is very boring.
 *
 */
public class SymbolInformation
{
	
	/*
	 * Requirements
	 * 	Store variable names
	 *  Store variable types
	 *  Store declaration locations
	 */
	
	/*
	 * Potential long-term refactoring project:
	 * Turn things like this and the class hierarchy project into 
	 * static classes and construct them directly from the parser?
	 */
	
	protected Identifier type;
	protected boolean inferred;
	
	public Identifier getType() {
		return type;
	}

	public void setType(Identifier type) {
		this.type = type;
	}

	/**
	 * 
	 * @param type
	 * @param inferred True if the type checker inferred the type of the variable,
	 * false if the type was read from the file. 
	 */
	public SymbolInformation(Identifier type, boolean inferred)
	{
		this.type = type;
		this.inferred = inferred;
	}
	
	public String toString()
	{
		return String.format("{type: %s}", type.getData());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SymbolInformation other = (SymbolInformation) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

    public boolean isInferred()
    {
        return inferred;
    }

    public void setInferred(boolean inferred)
    {
        this.inferred = inferred;
    }
}

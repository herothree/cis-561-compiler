package compiler;

import java.util.List;

import ast.ClassSignature;

public class BuildClassHierarchyVisitor implements QuackVisitor
{
	protected ClassHierarchyTree tree = new ClassHierarchyTree();
	
	public boolean preVisit(QuackAcceptor qa)
	{
		return true;
	}
	
	public void visit(QuackAcceptor qa)
	{
		if (qa instanceof ClassSignature)
		{
			ClassSignature cs = (ClassSignature) qa;
			tree.addClass(cs.getClassName(), cs.getParentClassName());
		}
	}
	
	public void postVisit(QuackAcceptor qa)
	{
		
	}
	
	public boolean isWellFormed()
	{
		return tree.isValid();
	}
	
	public ClassHierarchyTree getTree()
	{
		return this.tree;
	}
	
	public List<ClassHierarchyNode> getOrphans()
	{
		return this.tree.getOrphans();
	}
}

package compiler;

import java.util.ArrayList;
import java.util.List;

import ast.Identifier;

public class MethodInformation extends SymbolInformation {

	protected List<Identifier> argTypes;
	protected List<Identifier> argNames;
	
	// Used in code generation to determine where in the class it is
	// for overriding.
	protected int methodNumber;
	
	public MethodInformation(Identifier returnType) {
		this(returnType, new ArrayList<Identifier>(), new ArrayList<Identifier>());
	}
	
	public MethodInformation(Identifier returnType, List<Identifier> argTypes, List<Identifier> argNames)
	{
		super(returnType, false);
		this.argTypes = argTypes;
		this.argNames = argNames;
	}
	
	public MethodInformation(Identifier returnType, List<Identifier> argTypes, List<Identifier> argNames, int methodNumber)
	{
		this(returnType, argTypes, argNames);
		this.methodNumber = methodNumber;
	}
	
	
	
	public String toString()
	{
		StringBuilder result = new StringBuilder();
		result.append("{");
		result.append("arg types: ");
		for (Identifier t : argTypes)
		{
			result.append(t.getData());
			result.append(", ");
		}
		result.append("return type: ");
		result.append(this.type.getData());
		result.append("}");
		return result.toString();
	}

	public List<Identifier> getArgTypes() {
		return argTypes;
	}

	public void setArgTypes(List<Identifier> argTypes) {
		this.argTypes = argTypes;
	}

	public int getMethodNumber() {
		return methodNumber;
	}

	public void setMethodNumber(int methodNumber) {
		this.methodNumber = methodNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((argTypes == null) ? 0 : argTypes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MethodInformation other = (MethodInformation) obj;
		if (argTypes == null) {
			if (other.argTypes != null)
				return false;
		} else if (!argTypes.equals(other.argTypes))
			return false;
		return true;
	}

    public List<Identifier> getArgNames()
    {
        return argNames;
    }

    public void setArgNames(List<Identifier> argNames)
    {
        this.argNames = argNames;
    }

}

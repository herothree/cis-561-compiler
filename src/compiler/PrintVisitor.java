package compiler;

import java.util.Collections;

public class PrintVisitor implements QuackVisitor
{
	private static final int INDENTATION_LEVEL = 4;
	
	private int indent;
	
	public PrintVisitor()
	{
		// We add 4 to it before we visit anything
		this(-INDENTATION_LEVEL);
	}
	
	public PrintVisitor(int indent)
	{
		this.indent = indent;
	}
	
	public boolean preVisit(QuackAcceptor astn)
	{
		indent += INDENTATION_LEVEL;
		return true;
	}
	
	public void postVisit(QuackAcceptor astn)
	{
		indent -= INDENTATION_LEVEL;
	}
	
	public void visit(QuackAcceptor astn)
	{
		System.out.print(String.join("", Collections.nCopies(indent, " ")));
		System.out.println(astn);
	}

}

package compiler;

import java.util.ArrayList;
import java.util.List;

import ast.ConstructorCall;

public class CheckConstructorVisitor implements QuackVisitor {
	
	protected ClassHierarchyTree tree;
	
	protected List<String> invalidCalls;
	
	public CheckConstructorVisitor(ClassHierarchyTree tree)
	{
		this.tree = tree;
		this.invalidCalls = new ArrayList<String>();
	}
	
	@SuppressWarnings("unused")
	private CheckConstructorVisitor()
	{
		// Don't let this get used
	}
	
	public boolean isValid()
	{
		return invalidCalls.isEmpty();
	}
	
	public boolean preVisit(QuackAcceptor qa)
	{
		return true;
	}
	
	public void visit(QuackAcceptor qa)
	{
		if (qa instanceof ConstructorCall)
		{
			ConstructorCall cc = (ConstructorCall) qa;
			if (!tree.containsClass(cc.getClassName()))
			{
				String msg = "Invalid constructor call: " + cc.getClassName();
				invalidCalls.add(msg);
				if (QuackCompiler.PRINT_INFO)
				{
					System.out.println(msg);
				}
			}
		}
	}
	
	public ClassHierarchyTree getTree() {
		return tree;
	}

	public void setTree(ClassHierarchyTree tree) {
		this.tree = tree;
	}

	public List<String> getInvalidCalls() {
		return invalidCalls;
	}

	public void setInvalidCalls(List<String> invalidCalls) {
		this.invalidCalls = invalidCalls;
	}

	public void postVisit(QuackAcceptor qa)
	{
		
	}
}



package compiler;

import java.util.*;
import ast.*;
import errors.CompilerException;
import errors.IncompleteConstructorException;
import errors.IncorrectArgumentsException;
import errors.MismatchingOverrideException;
import errors.NonexistentMethodException;
import errors.TypeErrorException;

import javax.management.RuntimeErrorException;

public class TypeChecker
{

    // Represents that an expression can accept any type, and doesn't have one
    // yet
    public static final Identifier UNBOUND = new Identifier("$UNBOUND$");

    // How many errors can we catch before we just give up?
    public static final int MAX_ERRORS = 10;

    protected SymbolTable table;

    // How many times have we run the type checker?
    // It will have to be more than one in case the class declarations aren't in
    // order.
    protected int iteration;

    protected Program program;

    // Keep track of what class we're currently processing
    protected Identifier currentClass;

    // Keep track of what scope we're currently processing.
    // 0 corresponds to global scope.
    protected Stack<Integer> currentScope;

    // Keep track of what type we expect the method we're processing to return.
    protected Identifier currentReturnType;

    protected ClassHierarchyTree ct;

    protected Set<CompilerException> compilationErrors;

    // Remember if we need to run the type checker again.
    protected boolean runAgain;

    public TypeChecker(Program p)
    {
        // Starts with an empty symbol table, runs on a Program object (AST),
        // and fills up the symbol table with names and types while recursively
        // evaluating expressions.

        /*
         * Depth first search of the syntax tree, using a stack to keep track of
         * the current expression. Need multiple iterations to avoid class
         * declaration order problems.
         */

        this.program = p;
        this.iteration = 0;
        this.runAgain = true;
        this.currentClass = null;
        this.currentReturnType = null;
        this.currentScope = new Stack<Integer>();
        this.currentScope.push(0); // Start with 0 for global scope
        this.compilationErrors = new HashSet<CompilerException>();
        table = new SymbolTable();

        // Check the class structure and save it
        BuildClassHierarchyVisitor bv = new BuildClassHierarchyVisitor();
        program.accept(bv);
        CheckConstructorVisitor cv = new CheckConstructorVisitor(bv.getTree());
        this.ct = bv.getTree();

        // Make sure there are no errors within the class hierarchy.
        if (!bv.isWellFormed())
        {
            System.out.println(
                    "The following classes do not have parents within the class hierarchy:");
            for (ClassHierarchyNode chn : bv.getTree().getOrphans())
            {
                System.out.println(chn.getName().getData() + " (extends "
                        + chn.getParentName().getData() + ")");
            }
        }

        // Check for invalid constructor calls
        program.accept(cv);
        if (!cv.isValid())
        {
            for (String s : cv.getInvalidCalls())
            {
                if (QuackCompiler.PRINT_INFO)
                {
                    System.err.println(s);
                }
            }
        }
    }

    public Identifier getType(QuackAcceptor qa) throws TypeErrorException
    {

        if (QuackCompiler.DEBUG > 3)
        {
            System.out.println("Looking up type of: " + qa.toString());
        }

        if (qa instanceof Identifier)
        {
            Identifier id = (Identifier) qa;
            if (ct.containsClass(id))
            {
                return id;
            }
            else if (table.contains(currentScope.peek(), id))
            {
                return table.get(currentScope.peek(), id).type;
            }
            else if ("this".equals(id.getData()))
            {
                return currentClass;
            }
            else
            {
                return UNBOUND;
            }
        }

        if (qa instanceof IdLexpr)
        {
            return getType(((IdLexpr) qa).getIdent());
        }

        if (qa instanceof FieldLexpr)
        {
            FieldLexpr fdl = (FieldLexpr) qa;

            if (this.currentClass != null && (fdl.getClassName() instanceof IdLexpr)
                    && ((IdLexpr) fdl.getClassName()).getIdent().getData().equals("this"))
            {
                ((IdLexpr) fdl.getClassName()).setIdent(this.currentClass);
            }

            if (this.table.contains(fdl.getFieldName(), getType(fdl.getClassName())))
            {
                return this.table.get(fdl.getFieldName(), getType(fdl.getClassName())).type;
            }
            else
            {
                return UNBOUND;
            }
        }

        if (qa instanceof Binop)
        {

            Binop b = (Binop) qa;

            // Binary operators are handled by methods of the same name
            // as their operator (e.g. x + y -> x.PLUS(y)). So create a new
            // method
            // call out of the binop and return its type.
            return getType(new MethodCall(b.getR1(), b.getOp().toString(),
                    Arrays.asList(new Rexpr[] { new IdLexpr(getType(b.getR2()), b.getLocation()) }),
                    b.getLocation()));
        }

        if (qa instanceof Unop)
        {
            return getType(((Unop) qa).getR1());
        }

        if (qa instanceof ConstructorCall)
        {
            ConstructorCall cc = (ConstructorCall) qa;

            // Check if method exists in the global namespace
            if (!table.contains(0, cc.getClassName()))
            {
                throw new NonexistentMethodException(
                        String.format("The constructor %s does not exist", cc.getClassName()),
                        cc.getLocation());
            }

            List<Identifier> expectedArgTypes = table.getMethod(0, cc.getClassName()).getArgTypes();

            // Check if args match expected types
            if (expectedArgTypes.size() != cc.getActualArgs().size())
            {
                throw new IncorrectArgumentsException(String.format(
                        "%s expected %d argument(s), got %d.", cc.getClassName().getData(),
                        expectedArgTypes.size(), cc.getActualArgs().size()), cc.getLocation());
            }

            for (int i = 0; i < expectedArgTypes.size(); ++i)
            {
                ct.checkSubclass(getType(cc.getActualArgs().get(i)), expectedArgTypes.get(i),
                        cc.getLocation());
            }

            // Check if return type matches expected type
            return table.getMethod(0, cc.getClassName()).getType();
        }
        if (qa instanceof IntConst)
        {
            return new Identifier("Int");
        }
        if (qa instanceof BoolConst)
        {
            return new Identifier("Boolean");
        }
        if (qa instanceof MethodCall)
        {

            // rexpr := rexpr . ident ( actual_args )

            // This method must be able to reduce an rexpr. But, an rexpr
            // might have a type error in it. :(

            MethodCall mc = (MethodCall) qa;

            // Check if class is in symbol table
            Identifier caller = getType(mc.getCallingInstance());
            if (!table.contains(currentScope.peek(), caller))
            {
                // Something broke if this function returned a non-existent
                // caller.
                assert false;
            }

            // Check if method is in class
            if (null == getMethod(mc.getMethodName(), caller))
            {
                // We've got a method that's not in the class
                throw new NonexistentMethodException(
                        String.format("The class %s does not contain the method %s",
                                caller.getData(), mc.getMethodName().getData()),
                        mc.getLocation());
            }

            MethodInformation mInfo = getMethod(mc.getMethodName(), caller);

            // Check if args match expected types
            if (mInfo.getArgTypes().size() != mc.getActualArgs().size())
            {
                throw new IncorrectArgumentsException(String.format(
                        "%s expected %d argument(s), got %d.", mc.getMethodName().getData(),
                        mInfo.getArgTypes().size(), mc.getActualArgs().size()), mc.getLocation());
            }

            for (int i = 0; i < mInfo.getArgTypes().size(); ++i)
            {
                ct.checkSubclass(getType(mc.getActualArgs().get(i)), mInfo.getArgTypes().get(i),
                        mc.getLocation());
            }

            // If everything above this is ok, send back the return type of this
            // method.
            return mInfo.getType();
        }
        if (qa instanceof StringConst)
        {
            return new Identifier("String");
        }
        throw new RuntimeErrorException(null,
                "The type checker is not yet complete for type " + qa.getClass().toString());
        // return null;
    }

    /**
     * Returns the method information for a method if that method is in either
     * the given class or a parent of that class.
     * @param methodName The method name to look up.
     * @param caller The type of the object making the call
     * @return null if the method is not found, or the MethodInformation if it
     *         is.
     */
    public MethodInformation getMethod(Identifier methodName, Identifier caller)
    {
        MethodInformation result = null;
        Identifier current = caller;
        while (null != current && !table.isMethod(methodName, current))
        {
            current = ct.getParent(current);
        }

        if (null != current)
        {
            result = table.getMethod(methodName, current);
        }

        return result;
    }

    /**
     * Returns the name of the parent class that implements the given method.
     * @param methodName
     * @param caller
     * @return null if the method cannot be found in any parent class
     */
    public Identifier getMethodOrigin(Identifier methodName, Identifier caller)
    {
        Identifier current = caller;
        while (null != current && !table.isMethod(methodName, current))
        {
            current = ct.getParent(current);
        }

        return current;
    }

    private boolean run()
    {
        return this.run(this.program);
    }

    // This method is important
    private boolean run(QuackAcceptor qa)
    {

        /*
         * Algorithm outline for this method Recurse down to the statement level
         * of the AST. For a given statement, call the getType method on the
         * expressions in the statement, which recursively evaluates the
         * expression to determine the final type. Ensure that the type matches
         * the expected type (Boolean for if statements, etc). Also add
         * variables and methods to the symbol table as they are encountered.
         * 
         * Return false if we encounter too many type errors to keep going,
         * otherwise return true.
         */

        if (null == qa)
        {
            return true;
        }

        List<QuackAcceptor> children = qa.getChildren();

        try
        {

            // Process any statements that we come across
            if (qa instanceof AssignmentStatement)
            {
                AssignmentStatement as = (AssignmentStatement) qa;
                boolean inferred = true;

                // For assignment statements, make sure the types on
                // both sides match and save the resulting type in
                // the symbol table.

                Identifier typeLeft;

                // Was the type declared in the statement (x : Int = 5;)?
                if (!as.getType().equals(UNBOUND))
                {
                    inferred = false;
                    typeLeft = as.getType();

                    // First, make sure the type we got from the syntax is a
                    // valid type
                    ct.checkType(typeLeft, as.getLocation());

                    // Now, make sure that looking up the type of the arg
                    // doesn't
                    // yield a different result.
                    // (Could happen if a type is declared as different things
                    // at different times)
                    Identifier tmp = getType(as.getAssignee());
                    if (!tmp.equals(UNBOUND) && !ct.isSubclass(tmp, typeLeft))
                    {
                        throw new TypeErrorException("Cannot redefine type of existing variable.",
                                as.getLocation());
                    }

                }
                else
                {
                    // If not, check on the type in the symbol table
                    typeLeft = getType(as.getAssignee());
                }

                Identifier typeRight = getType(as.getToAssign());

                if (typeLeft.equals(UNBOUND))
                {
                    // If the left side doesn't have a type yet, use type
                    // inference to set it to whatever the right side's type is.
                    typeLeft = typeRight;
                }

                if (as.getAssignee() instanceof IdLexpr)
                {
                    IdLexpr idx = (IdLexpr) as.getAssignee();
                    if (!table.contains(currentScope.peek(), idx.getIdent()))
                    {
                        table.addSymbol(currentScope.peek(), idx.getIdent(),
                                new SymbolInformation(typeLeft, inferred));
                    }
                    else if (table.get(currentScope.peek(), idx.getIdent()).isInferred())
                    {
                        table.get(currentScope.peek(), idx.getIdent())
                                .setType(ct.findLowestCommonAncestor(typeLeft, typeRight));
                        typeLeft = typeRight = ct.findLowestCommonAncestor(typeLeft, typeRight);
                    }
                }

                if (as.getAssignee() instanceof FieldLexpr)
                {
                    FieldLexpr fldx = (FieldLexpr) as.getAssignee();
                    if (!table.contains(fldx.getFieldName(), getType(fldx.getClassName())))
                    {
                        table.addSymbol(fldx.getFieldName(), getType(fldx.getClassName()),
                                new SymbolInformation(typeLeft, inferred));
                    }
                    else if (table.get(fldx.getFieldName(), getType(fldx.getClassName()))
                            .isInferred())
                    {
                        table.get(fldx.getFieldName(), getType(fldx.getClassName()))
                                .setType(ct.findLowestCommonAncestor(typeLeft, typeRight));
                        typeLeft = typeRight = ct.findLowestCommonAncestor(typeLeft, typeRight);
                    }
                }

                // If right is not a subclass of left, we have a problem.
                if (!ct.isSubclass(typeRight, typeLeft))
                {
                    if (typeLeft == UNBOUND && typeRight == UNBOUND)
                    {
                        throw new TypeErrorException(
                                String.format("Error: variable \"%s\" used before initialization.",
                                        as.getToAssign()),
                                as.getLocation());
                    }
                    else
                    {
                        // Throws an exception so it can be added to the error
                        // list.
                        ct.checkSubclass(typeRight, typeLeft, as.getLocation());
                    }
                }

                // Assignment statement has now been verified, so return.
                // (no need to travel farther down the tree)
                return true;
            }
            else if (qa instanceof ClassDef)
            {
                ClassDef cd = (ClassDef) qa;

                if (cd.getScope() == -1)
                {
                    this.currentScope.push(UniqueID.getNext());
                    cd.setScope(currentScope.peek());
                }
                else
                {
                    // We already know the scope from a previous run
                    this.currentScope.push(cd.getScope());
                }
                
                // If we're entering into a class definition, save
                // the name of the class so we know what "this" is
                assert this.currentClass == null;
                this.currentClass = cd.getClassSignature().getClassName();
            }
            else if (qa instanceof ClassSignature)
            {
                ClassSignature cs = (ClassSignature) qa;

                // Add in the args to the constructor with the
                // current scope.
                // Also add the constructor itself to the symbol table as a
                // method.
                List<Identifier> argTypes = new ArrayList<Identifier>();
                List<Identifier> argNames = new ArrayList<Identifier>();
                for (FormalArg fa : cs.getFormalArgs())
                {
                    ct.checkType(fa.getArgType(), fa.getLocation());
                    table.addSymbol(currentScope.peek(), fa.getArgName(),
                            new SymbolInformation(fa.getArgType(), false));
                    argTypes.add(fa.getArgType());
                    argNames.add(fa.getArgName());
                }

                table.addSymbol(0, cs.getClassName(),
                        new MethodInformation(cs.getClassName(), argTypes, argNames));

                return true;
            }
            else if (qa instanceof ConstructorCall)
            {
                return true;
            }
            else if (qa instanceof ElifBlock)
            {
                ElifBlock eb = (ElifBlock) qa;
                ct.checkSubclass(getType(eb.getCondition()), new Identifier("Boolean"),
                        eb.getLocation());
            }
            else if (qa instanceof ElseBlock)
            {
                // Nothing to do here, children will be processed at the
                // end of this if/else tree.
            }
            else if (qa instanceof ExprStatement)
            {
                ExprStatement exprs = (ExprStatement) qa;

                // Make sure there are no type errors
                getType(exprs.getToExecute());

                // ExprStatement has been verified, so return true.
                return true;
            }
            else if (qa instanceof IfStatement)
            {
                IfStatement ifs = (IfStatement) qa;
                ct.checkSubclass(getType(ifs.getCondition()), new Identifier("Boolean"),
                        ifs.getLocation());

            }
            else if (qa instanceof MethodDef)
            {

                assert (null != currentClass);

                assert this.currentScope.peek() == 0;

                MethodDef md = (MethodDef) qa;

                if (md.getScope() == -1)
                {
                    // If we're entering the definition of a method,
                    // save a unique int as the scope of the current method
                    // so we can uniquely distinguish it in the symbol table
                    this.currentScope.push(UniqueID.getNext());
                    md.setScope(this.currentScope.peek());
                }
                else
                {
                    // We already know the scope from a previous run
                    this.currentScope.push(md.getScope());
                }

                this.currentReturnType = md.getReturnType();

                // Add the method to the symbol table
                // Note that formal args must have types
                ArrayList<Identifier> argTypes = new ArrayList<Identifier>();
                ArrayList<Identifier> argNames = new ArrayList<Identifier>();
                for (FormalArg fa : md.getFormalArgs())
                {
                    argTypes.add(fa.getArgType());
                    argNames.add(fa.getArgName());
                    table.addSymbol(currentScope.peek(), fa.getArgName(),
                            new SymbolInformation(fa.getArgType(), false));
                }
                table.addSymbol(md.getMethodName(), currentClass,
                        new MethodInformation(md.getReturnType(), argTypes, argNames));
            }
            else if (qa instanceof ReturnStatement)
            {
                ReturnStatement rs = (ReturnStatement) qa;
                ct.checkSubclass(getType(rs.getResult()), currentReturnType, rs.getLocation());
                return true;
            }
            else if (qa instanceof WhileStatement)
            {
                WhileStatement ws = (WhileStatement) qa;
                ct.checkSubclass(getType(ws.getCondition()), new Identifier("Boolean"),
                        ws.getLocation());
            }

            // then iterate through its children
            for (QuackAcceptor child : children)
            {
                if (!run(child))
                {
                    return false;
                }
            }

            // Clean up state when leaving certain types
            if (qa instanceof ClassDef)
            {
                // Reset to null when leaving a class
                this.currentClass = null;
                this.currentScope.pop();
                this.currentReturnType = null;
            }
            if (qa instanceof MethodDef)
            {
                this.currentScope.pop();
            }

        }
        catch (CompilerException ex)
        {
            compilationErrors.add(ex);

            if (compilationErrors.size() > MAX_ERRORS)
            {
                printErrors();

                return false;
            }
        }

        return true;
    }

    public SymbolInformation varInParentClass(Identifier currentClass, Identifier varName)
    {
        // SmartRobot . DestroyCity

        SymbolInformation result = null;
        Identifier current = currentClass;
        while (!ct.getRoot().getName().equals(current) && null == result)
        {
            current = ct.getParent(current);
            result = table.get(varName, current);
        }

        return result;
    }

    private boolean inheritanceCheck(Identifier child, Identifier parent)
    {
        // Given: parent is parent class of child

        // Make sure all variables initialized in parent class
        // are initialized in child class
        Set<FormalArg> parentVars = table.getAllMemberVariables(parent);
        Set<FormalArg> childVars = table.getAllMemberVariables(child);
        parentVars.removeAll(childVars);
        if (!parentVars.isEmpty())
        {
            StringBuilder msg = new StringBuilder();
            msg.append(String.format(
                    "The following variables are initialized in constructor "
                            + "\"%s\" but not in subclass constructor \"%s\": ",
                    parent.getData(), child.getData()));
            for (FormalArg fa : parentVars)
            {
                msg.append("\n\t");
                msg.append(fa.toString());
            }
            compilationErrors.add(new IncompleteConstructorException(msg.toString(), null));
        }

        // Make sure, if a method is also in a parent class, that it has the
        // same
        // argument and return types.
        Set<Identifier> parentMethods = table.getAllMemberMethods(parent);
        Set<Identifier> childMethods = table.getAllMemberMethods(child);

        for (Identifier method : childMethods)
        {
            if (parentMethods.contains(method))
            {
                MethodInformation parM = table.getMethod(method, parent);
                MethodInformation chiM = table.getMethod(method, child);

                // Methods must have the same arg types, and the child return
                // type
                // must be a subclass of the parent return type.
                if (!(parM.argTypes.equals(chiM.argTypes)
                        && ct.isSubclass(chiM.getType(), parM.getType())))
                {
                    compilationErrors.add(
                            new MismatchingOverrideException(
                                    String.format(
                                            "Overriding method \"%s\" in \"%s\" does"
                                                    + " not match method signature in parent class \"%s\".",
                                            method.getData(), child.getData(), parent.getData()),
                                    null));
                }
            }
        }

        return true;
    }

    /**
     * Performs type checking on the AST
     * 
     * @return true if the hierarchy is well formed, false otherwise.
     */
    public boolean typeCheck()
    {
        // Starting point for everything that goes on here.

        boolean result = true;

        /* 
         * Occasionally, type inference can take a few iterations to
         * backpropogate. For instance, in the code:
         * 
         * a = 5;
         * b = 7;
         * a = b;
         * b = "txt;
         * 
         * A will be an int after one run and an Obj after two. If more variables are involved,
         * more iterations are needed to catch the errors.
         */
        for (int i = 0; i < 10; ++i)
        {
            result = run();
        }
        
        List<Identifier> allClassNames = ct.getAllClassNames();
        for (int i = 0; i < allClassNames.size(); ++i)
        {
            for (int j = 0; j < allClassNames.size(); ++j)
            {
                if (ct.isSubclass(allClassNames.get(i), allClassNames.get(j))
                        && !allClassNames.get(j).equals(new Identifier("Obj"))
                        && !allClassNames.get(i).equals(new Identifier("Obj")))
                {
                    inheritanceCheck(allClassNames.get(i), allClassNames.get(j));
                }
            }
        }

        // If result is false, we've already given up and printed
        // all the errors we found.
        if (result && !compilationErrors.isEmpty())
        {
            result = false;
            printErrors();
        }
        else
        {
            // Our class passed the type checker. Populate the methodTable for
            // code generation purposes
            // Populate the method table with all classes

            // Not sure exactly where the best place is for this code, but
            // everything
            // it needs is available here so why not.
            for (Identifier className : ct.getAllClassNames())
            {
                table.getMethodTable().addClass(className, ct.getParent(className));
                for (Identifier methodName : table.getAllMemberMethods(className))
                {
                    table.getMethodTable().addMethod(className,
                            getMethodOrigin(methodName, className), methodName);
                }

                table.getMemberVarTable().addClass(className, ct.getParent(className));
                for (FormalArg fa : table.getAllMemberVariables(className))
                {
                    table.getMemberVarTable().addMethod(className, className, fa.getArgName());
                }
            }

        }
        return result;
    }

    public void printErrors()
    {
        int loc = 0;

        if (QuackCompiler.PRINT_INFO)
        {
            if (compilationErrors.size() > 0)
            {
                System.err.printf("Encountered %d error(s):\n", compilationErrors.size());
            }

            for (CompilerException e : compilationErrors)
            {
                loc = (null == e.getLocation()) ? 0 : e.getLocation().getLine();
                System.err.println(loc + ": " + e.getMessage());
            }
        }
    }

    public SymbolTable getTable()
    {
        return table;
    }

    public void setTable(SymbolTable table)
    {
        this.table = table;
    }

    public ClassHierarchyTree getCt()
    {
        return ct;
    }

    public void setCt(ClassHierarchyTree ct)
    {
        this.ct = ct;
    }

    public Stack<Integer> getCurrentScope()
    {
        return currentScope;
    }

    public Identifier getCurrentClass()
    {
        return currentClass;
    }

    public void setCurrentClass(Identifier currentClass)
    {
        this.currentClass = currentClass;
    }
}

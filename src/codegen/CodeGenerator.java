package codegen;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import javax.management.RuntimeErrorException;

import ast.*;
import compiler.*;
import errors.*;

public class CodeGenerator
{
    public static final String MAIN_CLASSNAME = "MyMain";

    // This compiler generates c code. What should the intermediate
    // .c and .h files be called?
    private static final String SOURCE_FILENAME = "_qk_intermediate.c";
    private static final String HEADER_FILENAME = "_qk_intermediate.h";

    protected TypeChecker tc;
    protected Program program;
    protected PrintStream source;
    protected PrintStream header;
    protected List<String> preamble_c;
    protected List<String> preamble_h;
    protected Path outputFilename;
    protected MethodGenInfo methodGenInfo;
    protected Stack<String> if_ids;
    protected Stack<String> while_ids;

    public CodeGenerator(TypeChecker tc, Program program, String outputFilename)
    {
        this.tc = tc;
        this.program = program;
        this.source = null;
        this.outputFilename = Paths.get(outputFilename);
        this.if_ids = new Stack<String>();
        this.while_ids = new Stack<String>();
        this.preamble_c = new ArrayList<String>();        
        this.preamble_h = new ArrayList<String>();
        
        try
        {
            //preamble_c = Files.readAllLines(Paths.get(PREAMBLE_C_FILENAME), StandardCharsets.UTF_8);
            //preamble_h = Files.readAllLines(Paths.get(PREAMBLE_H_FILENAME), StandardCharsets.UTF_8);
            
            
            
            // Create the output source and header files
            this.source = new PrintStream(
                    new File(SOURCE_FILENAME));
            this.header = new PrintStream(
                    new File(HEADER_FILENAME));


        }
        catch (IOException e)
        {
            e.printStackTrace();
            System.exit(-1);
        }

    }

    public void compile()
    {
        List<String> commands = new ArrayList<String>();

        commands.add("gcc");
        commands.add("-Wall");
        commands.add("-g");
        commands.add("-o");
        commands.add(outputFilename.toString());
        commands.add(SOURCE_FILENAME);

        SystemCommandExecutor commandExecutor = new SystemCommandExecutor(commands);
        try
        {
            int result = commandExecutor.executeCommand();
            
            if (0 != result)
            {
                System.out.println("Output:\n" + commandExecutor.getStandardOutputFromCommand());
                System.out.println("Output:\n" + commandExecutor.getStandardErrorFromCommand());
                System.err.println("Compilation exit status: " + result);
                System.exit(result);
            }

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public void execute()
    {
        List<String> commands = new ArrayList<String>();
        commands.add(String.format("%s", outputFilename));

        try
        {
            SystemCommandExecutor commandExecutor = new SystemCommandExecutor(commands);
            int result = commandExecutor.executeCommand();
            if (0 != result)
            {
                System.err.println("Compilation exit status: " + result);
                System.exit(result);
            }

            System.out.println("Output:\n" + commandExecutor.getStandardOutputFromCommand());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    // Starting point for everything that goes on here
    public void generate()
    {
        long starttime = System.nanoTime();

        for (String line : preamble_c)
        {
            source.println(line);
        }

        // Print the first 14 lines of the header preamble to get
        // header guards and forward declarations of the builtin methods
        for (int i = 0; i < 14; ++i)
        {
            header.println(preamble_h.get(i));
        }

        // Do all the code generation
        run();

        source.println("\nint main(int argc, char* argv[]) {");
        source.printf("    obj_%s m = the_class_%s->constructor();\n", MAIN_CLASSNAME,
                MAIN_CLASSNAME);
        source.printf("    printf(\"\\n\");\n"); // Make sure the output ends
                                                 // with a newline.
        source.println("}\n\n");
        source.close();

        // Print the rest of the header file
        for (int i = 14; i < preamble_h.size(); ++i)
        {
            header.println(preamble_h.get(i));
        }
        header.println("#endif\n");
        header.close();

        long endtime = System.nanoTime();

        //System.out.printf("Code generation completed in %d ms\n\n", (endtime - starttime) / 1000000);
        
        compile();
        //execute(); // Uncomment this to run the program automatically after compiling it.
    }

    /*
     * Should be used for variable names that are present in the quack program
     */
    protected String getVarName(Identifier id)
    {
        // Special cases
        if (id.getData().equals("this"))
        {
            return "this";
        }
        if (id.getData().equals("Nothing"))
        {
            return "NULL";
        }
        return id.getData() + "_ident";
    }

    private boolean run()
    {
        return run(this.program);
    }

    private boolean run(QuackAcceptor qa)
    {

        /*
         * Algorithm outline for this method Recurse down to the statement level
         * of the AST. For a given statement, call the getType method on the
         * expressions in the statement, which recursively evaluates the
         * expression to determine the final type. Ensure that the type matches
         * the expected type (Boolean for if statements, etc). Also add
         * variables and methods to the symbol table as they are encountered.
         * 
         * Return false if we encounter too many type errors to keep going,
         * otherwise return true.
         */

        if (null == qa)
        {
            return true;
        }

        List<QuackAcceptor> children = qa.getChildren();

        try
        {

            // Process any statements that we come across
            if (qa instanceof AssignmentStatement)
            {
                AssignmentStatement as = (AssignmentStatement) qa;

                // Get the variable name to use for the right side
                String leftSide = getTmpName(as.getAssignee());

                // Get the variable name to use for the left side
                String rightSide = getTmpName(as.getToAssign());

                // Generate the code
                methodGenInfo.getLines().add(String.format("    %s = %s;", leftSide, rightSide));

            }
            else if (qa instanceof ClassBody)
            {

                // Generate class struct declaration
                source.printf("struct class_%s_struct;\n", tc.getCurrentClass());
                source.printf("typedef struct class_%s_struct* class_%s;\n", tc.getCurrentClass(),
                        tc.getCurrentClass());

                // Generate instance struct definition
                source.printf("typedef struct obj_%s_struct {\n", tc.getCurrentClass());
                source.printf("    class_%s  clazz;\n", tc.getCurrentClass());

                // Generate declarations for member variables
                // for (FormalArg fa :
                // tc.getTable().getAllMemberVariables(currentClass))
                for (Pair p : tc.getTable().getMemberVarTable().get(tc.getCurrentClass()))
                {
                    source.printf("    obj_%s %s;\n",
                            tc.getTable().get(p.getMethodName(), tc.getCurrentClass()).getType(),
                            p.getMethodName());
                }
                source.printf("} * obj_%s;\n", tc.getCurrentClass());

                // Forward declare class struct
                source.printf("struct  class_%s_struct  the_class_%s_struct;\n",
                        tc.getCurrentClass(), tc.getCurrentClass());

                // Generate class struct definition body
                source.printf("struct class_%s_struct {\n", tc.getCurrentClass());

                // Generate the constructor pointer declaration in the
                // class struct definition
                MethodInformation mi = tc.getTable().getConstructor(tc.getCurrentClass());
                source.printf("    obj_%s (*%s) (", mi.getType(), "constructor");
                for (int i = 0; i < mi.getArgTypes().size(); ++i)
                {
                    source.printf("obj_%s", mi.getArgTypes().get(i));
                    if (i + 1 != mi.getArgTypes().size())
                    {
                        source.printf(", ");
                    }

                }
                source.printf(");\n");

                // Generate the signatures for the methods in the class struct
                // definition
                for (Pair p : tc.getTable().getMethodTable().get(tc.getCurrentClass()))
                {
                    mi = tc.getTable().getMethod(p.getMethodName(), p.getOriginClass());
                    source.printf("    obj_%s %s (obj_%s this", mi.getType(),
                            String.format("(*%s)", p.getMethodName()), p.getOriginClass());
                    if (mi.getArgTypes().size() > 0)
                    {
                        source.printf(",");
                    }
                    for (int i = 0; i < mi.getArgTypes().size(); ++i)
                    {
                        source.printf("obj_%s %s", mi.getArgTypes().get(i),
                                getVarName(mi.getArgNames().get(i)));
                        if (i + 1 != mi.getArgTypes().size())
                        {
                            source.printf(", ");
                        }

                    }
                    source.printf(");\n");
                }
                source.printf("};\n");

                // Forward declare instance of class struct
                source.printf("extern class_%s the_class_%s;\n", tc.getCurrentClass(),
                        tc.getCurrentClass());

                // Generate constructor signature
                mi = tc.getTable().getConstructor(tc.getCurrentClass());
                source.printf("    obj_%s new_%s (", tc.getCurrentClass(), tc.getCurrentClass());
                for (int i = 0; i < mi.getArgTypes().size(); ++i)
                {
                    source.printf("obj_%s %s", mi.getArgTypes().get(i),
                            getVarName(mi.getArgNames().get(i)));
                    if (i + 1 != mi.getArgTypes().size())
                    {
                        source.printf(", ");
                    }
                }
                source.printf(")");

                source.printf("\n");

                // Generate constructor body
                source.printf("{\n");
                source.printf("    obj_%s this = (obj_%s) malloc(sizeof(struct obj_%s_struct));\n",
                        tc.getCurrentClass(), tc.getCurrentClass(), tc.getCurrentClass());
                source.printf("    this->clazz = the_class_%s;\n", tc.getCurrentClass());

                // Remove constructor statements from the children list (the
                // list of
                // AST nodes to process next) to process them here.
                ASTNodeList<Statement> constructorBody = new ASTNodeList<Statement>();
                while (!children.isEmpty() && children.get(0) instanceof Statement)
                {
                    // Ugh inefficient but whatever
                    constructorBody.add((Statement) children.remove(0));
                }

                // Setup MethodGenInfo
                methodGenInfo = new MethodGenInfo();
                for (FormalArg fa : tc.getTable().getAllScopeVariables(tc.getCurrentScope().peek()))
                {
                    // Don't print declarations for variables that are
                    // parameters
                    if (!mi.getArgNames().contains(fa.getArgName()))
                    {
                        methodGenInfo.getVariables().add(new FormalArg(getVarName(fa.getArgName()),
                                fa.getArgType().getData(), null));
                    }
                }
                // Generate the constructor code
                run(constructorBody);

                // Teardown MethodGenInfo
                methodGenInfo.write(source);
                methodGenInfo = null;

                source.printf("    return this;\n");
                source.printf("}\n");

                // Create instance of class struct
                /* The Pt Class (a singleton) */
                source.printf("struct  class_%s_struct  the_class_%s_struct = {\n",
                        tc.getCurrentClass(), tc.getCurrentClass());
                source.printf("    new_%s,     /* Constructor */\n", tc.getCurrentClass());

                for (Pair p : tc.getTable().getMethodTable().get(tc.getCurrentClass()))
                {
                    source.printf("%s_method_%s,\n", p.getOriginClass(), p.getMethodName());
                }

                source.printf("};\n");

                source.printf("class_%s the_class_%s = &the_class_%s_struct;\n\n\n",
                        tc.getCurrentClass(), tc.getCurrentClass(), tc.getCurrentClass());

                // Place a forward declaration of the struct in the header file
                header.printf("struct obj_%s_struct;\n", tc.getCurrentClass());
                header.printf("typedef struct obj_%s_struct* obj_%s;\n\n", tc.getCurrentClass(),
                        tc.getCurrentClass());
            }
            else if (qa instanceof ClassDef)
            {
                ClassDef cd = (ClassDef) qa;
                tc.getCurrentScope().push(cd.getScope());
                tc.setCurrentClass(cd.getClassSignature().getClassName());

            }
            else if (qa instanceof ClassSignature)
            {
                // Do nothing, ClassDef and ClassBody handle everything
                return true;
            }
            else if (qa instanceof ConstructorCall)
            {
                // Seems like this is taken care of elsewhere
                return true;
            }
            else if (qa instanceof ElifBlock)
            {
                // These are processed in the if statement block
                return true;
            }
            else if (qa instanceof ElseBlock)
            {
                // These are processed in the if statement block
                return true;
            }
            else if (qa instanceof ExprStatement)
            {
                ExprStatement exprs = (ExprStatement) qa;
                getTmpName(exprs.getToExecute());
                return true;
            }
            else if (qa instanceof IfStatement)
            {
                IfStatement ifs = (IfStatement) qa;
                String condition = getTmpName(ifs.getCondition());
                if_ids.push(Integer.toString(UniqueID.getNext()));
                methodGenInfo.getLines().add(String.format("if_start_%s: ", if_ids.peek()));
                String next;
                int elifCounter = 0;

                if (ifs.getElifBlocks().size() > 0)
                {
                    next = "elif_" + if_ids.peek() + "_" + elifCounter;
                }
                else if (ifs.getElseBlock() != null)
                {
                    next = "else_" + if_ids.peek();
                }
                else
                {
                    next = "if_end_" + if_ids.peek();
                }
                methodGenInfo.getLines()
                        .add(String.format("    if (!%s->value) {goto %s;}", condition, next));

                for (QuackAcceptor stmt : ifs.getStmts().getChildren())
                {
                    run(stmt);
                }

                // If we reach this part normally, jump to the end
                methodGenInfo.getLines().add(String.format("    goto if_end_%s;", if_ids.peek()));

                for (ElifBlock eb : ifs.getElifBlocks())
                {
                    methodGenInfo.getLines()
                            .add(String.format("    elif_%s_%d:", if_ids.peek(), elifCounter));
                    elifCounter += 1;

                    if (ifs.getElifBlocks().size() > elifCounter)
                    {
                        next = "elif_" + if_ids.peek() + "_" + elifCounter;
                    }
                    else if (ifs.getElseBlock() != null)
                    {
                        next = "else_" + if_ids.peek();
                    }
                    else
                    {
                        next = "if_end_" + if_ids.peek();
                    }
                    condition = getTmpName(eb.getCondition());
                    methodGenInfo.getLines()
                            .add(String.format("    if (!%s->value) {goto %s;}", condition, next));

                    // Do all statements
                    for (QuackAcceptor stmt : eb.getStatementBlock().getChildren())
                    {
                        run(stmt);
                    }

                    // skip to done
                    methodGenInfo.getLines()
                            .add(String.format("    goto if_end_%s;", if_ids.peek()));
                }

                methodGenInfo.getLines().add(String.format("    goto if_end_%s;", if_ids.peek()));
                methodGenInfo.getLines().add(String.format("    else_%s:\n", if_ids.peek()));

                if (ifs.getElseBlock() != null)
                {
                    for (QuackAcceptor stmt : ifs.getElseBlock().getStatementBlock().getChildren())
                    {
                        run(stmt);
                    }
                }
                methodGenInfo.getLines().add(String.format("    if_end_%s:;\n", if_ids.pop()));

                // Everything inside this if statement has been generated
                return true;
            }
            else if (qa instanceof MethodDef)
            {
                MethodDef md = (MethodDef) qa;
                tc.getCurrentScope().push(md.getScope());
                methodGenInfo = new MethodGenInfo();
                MethodInformation mi = tc.getTable().getMethod(md.getMethodName(),
                        tc.getCurrentClass());
                // generate method signature
                printMethodSignature(new Identifier(
                        String.format("%s_method_%s", tc.getCurrentClass(), md.getMethodName())),
                        mi, false, source);

                source.printf("{\n");

                for (FormalArg fa : tc.getTable().getAllScopeVariables(md.getScope()))
                {
                    // Don't print declarations for variables that are
                    // parameters
                    if (!mi.getArgNames().contains(fa.getArgName()))
                    {
                        methodGenInfo.getVariables().add(new FormalArg(getVarName(fa.getArgName()),
                                fa.getArgType().getData(), null));
                    }
                }
                
                // generate method body
                for (QuackAcceptor stmt : md.getStatements().getChildren())
                {
                    run(stmt);
                }
                methodGenInfo.write(source);
                source.printf("}\n");

                // Place a declaration at the end of the header file
                StringBuilder headerDeclaration = new StringBuilder();
                headerDeclaration.append(String.format("obj_%s %s (obj_%s this", mi.getType(),
                        String.format("%s_method_%s", tc.getCurrentClass(), md.getMethodName()),
                        tc.getCurrentClass()));
                if (mi.getArgTypes().size() > 0)
                {
                    headerDeclaration.append(",");
                }
                for (int i = 0; i < mi.getArgTypes().size(); ++i)
                {
                    headerDeclaration.append(String.format("obj_%s %s", mi.getArgTypes().get(i),
                            getVarName(mi.getArgNames().get(i))));
                    if (i + 1 != mi.getArgTypes().size())
                    {
                        headerDeclaration.append(", ");
                    }
                }
                headerDeclaration.append(");\n");
                preamble_h.add(headerDeclaration.toString());
            }
            else if (qa instanceof ReturnStatement)
            {
                ReturnStatement rs = (ReturnStatement) qa;
                if (null == rs.getResult())
                {
                    methodGenInfo.getLines().add("return;\n");
                }
                else
                {
                    String tmpName = getTmpName(rs.getResult());
                    methodGenInfo.getLines().add(String.format("return %s;\n", tmpName));
                }
                return true;
            }
            else if (qa instanceof WhileStatement)
            {
                WhileStatement ws = (WhileStatement) qa;
                while_ids.push(Integer.toString(UniqueID.getNext()));
                methodGenInfo.getLines().add(String.format("while_start_%s:\n", while_ids.peek()));
                String condition = getTmpName(ws.getCondition());
                methodGenInfo.getLines().add(String.format("    if (!%s->value) {goto %s;}\n",
                        condition, "while_end_" + while_ids.peek()));

                // Generate all the statements
                for (QuackAcceptor stmt : ws.getStatementBlock().getChildren())
                {
                    run(stmt);
                }

                methodGenInfo.getLines()
                        .add(String.format("    goto while_start_%s;\n", while_ids.peek()));
                methodGenInfo.getLines()
                        .add(String.format("    while_end_%s:;\n", while_ids.pop()));
                return true;
            }

            // then iterate through its children
            for (QuackAcceptor child : children)
            {
                if (!run(child))
                {
                    return false;
                }
            }

            // Clean up state when leaving certain types
            if (qa instanceof ClassDef)
            {
                tc.setCurrentClass(null);
                tc.getCurrentScope().pop();
            }
            else if (qa instanceof MethodDef)
            {
                tc.getCurrentScope().pop();
                methodGenInfo = null;
            }
            else if (qa instanceof IfStatement)
            {
                // always close your if statements. (doing this in the normal
                // processing now)
                // methodGenInfo.getLines().add(String.format(" end_%s:\n",
                // if_ids.pop()));
            }

        }
        catch (CompilerException ex)
        {
            System.err.println(ex.getMessage());
            return false;
        }

        return true;
    }

    private void printMethodSignature(Identifier name, MethodInformation mi,
            boolean includeSemicolon, PrintStream out)
    {
        out.printf("    obj_%s %s (obj_%s this", mi.getType(), name, tc.getCurrentClass());
        if (mi.getArgTypes().size() > 0)
        {
            out.printf(",");
        }
        for (int i = 0; i < mi.getArgTypes().size(); ++i)
        {
            // Name all the args "a1", "a2", etc since it doesn't matter here
            out.printf("obj_%s %s", mi.getArgTypes().get(i), getVarName(mi.getArgNames().get(i)));
            if (i + 1 != mi.getArgTypes().size())
            {
                out.printf(", ");
            }

        }
        out.printf(")");
        if (includeSemicolon)
        {
            out.printf(";");
        }
        out.printf("\n");
    }

    /*
     * Given some kind of expression, generates code to evaluate it and returns
     * the name of a temporary variable that contains the result.
     */
    public String getTmpName(QuackAcceptor qa) throws TypeErrorException
    {
        // This method is important

        if (qa instanceof Binop)
        {
            Binop b = (Binop) qa;
            if (b.getOp().equals(Operator.AND))
            {
                // generate first boolean
                String result = newTmpVar();
                methodGenInfo.getVariables().add(new FormalArg(result, "Boolean"));
                String first = getTmpName(b.getR1());

                // use C if statement
                methodGenInfo.getLines().add(String.format("    %s = %s;\n", result, first));
                methodGenInfo.getLines().add(String.format("    if (%s->value) {", first));

                // generate second boolean
                String second = getTmpName(b.getR2());
                methodGenInfo.getLines().add(String.format("    %s = %s;", result, second));

                // close if statement
                methodGenInfo.getLines().add(String.format("    }"));
                return result;
            }
            else if (b.getOp().equals(Operator.OR))
            {
                // generate first boolean
                String result = newTmpVar();
                methodGenInfo.getVariables().add(new FormalArg(result, "Boolean"));
                String first = getTmpName(b.getR1());

                // use C if statement
                methodGenInfo.getLines().add(String.format("    %s = %s;\n", result, first));
                methodGenInfo.getLines().add(String.format("    if (!%s->value) {", first));

                // generate second boolean
                String second = getTmpName(b.getR2());
                methodGenInfo.getLines().add(String.format("    %s = %s;", result, second));

                // close if statement
                methodGenInfo.getLines().add(String.format("    }"));
                return result;
            }
            else
            {
                String tmpName = newTmpVar();
                methodGenInfo.getVariables().add(new FormalArg(tmpName, tc.getType(b).getData()));
                String left = getTmpName(b.getR1());
                String right = getTmpName(b.getR2());
                methodGenInfo.getLines().add(String.format("    %s = %s->clazz->%s(%s, %s);",
                        tmpName, left, b.getOp().toString(), left, right));
                return tmpName;
            }

        }
        else if (qa instanceof BoolConst)
        {
            BoolConst bc = (BoolConst) qa;
            return (bc.getData()) ? "lit_true" : "lit_false";
        }

        else if (qa instanceof ConstructorCall)
        {
            ConstructorCall cc = (ConstructorCall) qa;
            String tmpName = newTmpVar();

            methodGenInfo.getVariables()
                    .add(new FormalArg(tmpName, tc.getType(cc.getClassName()).getData()));
            StringBuilder sb = new StringBuilder();
            sb.append(String.format("    %s = the_class_%s->constructor(", tmpName,
                    cc.getClassName()));
            for (Rexpr rx : cc.getActualArgs())
            {
                sb.append(getTmpName(rx));
                sb.append(",");
            }
            if (cc.getActualArgs().size() > 0)
            {
                sb.deleteCharAt(sb.length() - 1); // Remove the last comma
            }
            sb.append(");\n");

            methodGenInfo.getLines().add(sb.toString());
            return tmpName;
        }

        else if (qa instanceof FieldLexpr)
        {
            FieldLexpr fdl = (FieldLexpr) qa;
            String caller = getTmpName(fdl.getClassName());
            if (caller.equals(tc.getCurrentClass().getData()))
            {
                caller = "this";
            }
            String fld = fdl.getFieldName().getData();
            return String.format("%s->%s", caller, fld);
        }

        else if (qa instanceof Identifier)
        {
            Identifier id = (Identifier) qa;
            return getVarName(id);
        }

        else if (qa instanceof IdLexpr)
        {
            IdLexpr idl = (IdLexpr) qa;
            if (idl.getIdent().equals(tc.getCurrentClass()))
            {
                return "this";
            }

            // These are added to methodGenInfo directly from the symbol table
            // when methodGenInfo is created (this avoids scoping problems).
            return getVarName(idl.getIdent());
        }

        else if (qa instanceof IntConst)
        {
            String tmpName = newTmpVar();
            methodGenInfo.getVariables().add(new FormalArg(tmpName, tc.getType(qa).getData()));
            methodGenInfo.getLines().add(String.format("    %s = int_literal(%d);\n", tmpName,
                    ((IntConst) qa).getData()));
            return tmpName;
        }

        else if (qa instanceof MethodCall)
        {
            MethodCall mc = (MethodCall) qa;
            String tmpName = newTmpVar();
            methodGenInfo.getVariables().add(new FormalArg(tmpName, tc.getType(mc).getData()));

            // pt1->clazz->PRINT(pt1);
            StringBuilder call = new StringBuilder();
            String caller = getTmpName(mc.getCallingInstance());
            call.append(
                    String.format("    %s = %s->clazz->%s(", tmpName, caller, mc.getMethodName()));

            // Cast the argument to the type that defines the class to avoid
            // compiler warnings
            String originClass = tc
                    .getMethodOrigin(mc.getMethodName(), tc.getType(mc.getCallingInstance()))
                    .getData();
            call.append("(obj_" + originClass + ")");

            call.append(caller);

            for (int i = 0; i < mc.getActualArgs().size(); ++i)
            {
                call.append(", ");
                call.append(getTmpName(mc.getActualArgs().get(i)));
            }

            call.append(");\n");

            methodGenInfo.getLines().add(call.toString());
            return tmpName;
        }

        else if (qa instanceof StringConst)
        {
            String tmpName = newTmpVar();
            methodGenInfo.getVariables()
                    .add(new FormalArg(tmpName, tc.getType(qa).getData(), null));
            methodGenInfo.getLines().add(String.format("    %s = str_literal(%s);\n", tmpName,
                    ((StringConst) qa).getData()));
            return tmpName;
        }

        else if (qa instanceof Unop)
        {
            // Two cases: minus and not

            Unop uo = (Unop) qa;
            if (uo.getOp().equals(Operator.NOT))
            {
                String tmpName = newTmpVar();
                methodGenInfo.getVariables().add(new FormalArg(tmpName, "Boolean"));
                
                methodGenInfo.getLines()
                        .add(String.format("%s = (%s->value) ? lit_false : lit_true;\n", tmpName,
                                getTmpName(uo.getR1())));
                return tmpName;

            }
            else if (uo.getOp().equals(Operator.MINUS))
            {
                String tmpName = newTmpVar();
                methodGenInfo.getVariables()
                        .add(new FormalArg(tmpName, tc.getType(uo.getR1()).getData()));
                String left = getTmpName(new IntConst(-1, null));
                String right = getTmpName(uo.getR1());
                methodGenInfo.getLines().add(String.format("    %s = %s->clazz->%s(%s, %s);",
                        tmpName, left, Operator.TIMES, left, right));
                return tmpName;
            }
        }

        throw new RuntimeErrorException(null,
                "The code generator is not yet complete for type " + qa.getClass().toString());
    }

    private String newTmpVar()
    {
        return "tmp" + UniqueID.getNext();
    }

    public List<String> getPreamble_c()
    {
        return preamble_c;
    }

    public void setPreamble_c(List<String> preamble_c)
    {
        this.preamble_c = preamble_c;
    }

    public List<String> getPreamble_h()
    {
        return preamble_h;
    }

    public void setPreamble_h(List<String> preamble_h)
    {
        this.preamble_h = preamble_h;
    }

}

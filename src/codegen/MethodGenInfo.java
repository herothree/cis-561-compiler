package codegen;

import java.io.PrintStream;
import java.util.*;
import ast.*;

public class MethodGenInfo
{
    private List<String> lines;
    private Set<FormalArg> variables;
    
    public MethodGenInfo()
    {
        lines = new ArrayList<String>();
        variables = new HashSet<FormalArg>();
    }

    public List<String> getLines()
    {
        return lines;
    }

    public void setLines(List<String> lines)
    {
        this.lines = lines;
    }

    public Set<FormalArg> getVariables()
    {
        return variables;
    }

    public void setVariables(Set<FormalArg> variables)
    {
        this.variables = variables;
    }
    
    public void write(PrintStream out)
    {
        for (FormalArg fa : variables)
        {
            out.printf("    obj_%s %s;\n", fa.getArgType(), fa.getArgName());
        }
        out.println();
        
        for (String line : lines)
        {
            out.println(line);
        }
    }
}

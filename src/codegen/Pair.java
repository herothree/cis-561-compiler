package codegen;

import ast.Identifier;

public class Pair
{
    private Identifier originClass;
    private Identifier methodName;
    public Pair(Identifier originClass, Identifier methodName)
    {
        super();
        this.originClass = originClass;
        this.methodName = methodName;
    }
    public Pair copy()
    {
        Pair result = new Pair(new Identifier(originClass.getData()), 
                new Identifier(methodName.getData()));
        return result;
    }
    public Identifier getOriginClass()
    {
        return originClass;
    }
    public void setOriginClass(Identifier origin)
    {
        this.originClass = origin;
    }
    public Identifier getMethodName()
    {
        return methodName;
    }
    public void setMethodName(Identifier name)
    {
        this.methodName = name;
    }
    
    public String toString()
    {
        return String.format("%s (from %s)", getMethodName(), getOriginClass());
    }
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((methodName == null) ? 0 : methodName.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pair other = (Pair) obj;
        if (methodName == null)
        {
            if (other.methodName != null)
                return false;
        }
        else if (!methodName.equals(other.methodName))
            return false;
        return true;
    }
    
}

package codegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ast.Identifier;

/**
 * This class keeps track of the methods available to each object, what class
 * they were inherited from, and where they are in the order of methods in the object.
 *
 */
public class MethodTable
{
    private HashMap<Identifier, ArrayList<Pair>> table;
    
    public MethodTable()
    {
        table = new HashMap<Identifier, ArrayList<Pair>>();
    }
    
    
    
    /**
     * Add a class to the table. It will automatically inherit all methods from its parent class
     * @param className
     * @param parentClass
     */
    public void addClass(Identifier className, Identifier parentClass)
    {
        if (table.containsKey(className))
        {
            // We already have this class
            return;
        }
        
        // Object doesn't need to have a parent class.
        if (className.getData().equals("Obj"))
        {
            table.put(className, new ArrayList<Pair>());
        }
        else
        {
            table.put(className, new ArrayList<Pair>());
            for (Pair p : table.get(parentClass))
            {
                table.get(className).add(p.copy());
            }
        }
    }
    
    /**
     * Add a method to a class. It will be placed in the correct location, 
     * or appended to the list if it is not currently present. 
     * @param className
     * @param originClass
     * @param methodName
     */
    public void addMethod(Identifier className, Identifier originClass,Identifier methodName)
    {
        if (!table.containsKey(className))
        {
            throw new RuntimeException(String.format("Class \"%s\" must be contained in table.", className));
        }
        
        // Note that pairs are compared for equality based only on the method name. 
        Pair toAdd = new Pair(originClass, methodName);
        
        if (table.get(className).contains(toAdd))
        {
            int loc = table.get(className).indexOf(toAdd);
            table.get(className).get(loc).setOriginClass(originClass);
        }
        else
        {
            table.get(className).add(toAdd);
        }
    }
    
    public int getMethodIndex(Identifier className, Identifier methodName)
    {
        if (!table.containsKey(className))
        {
            throw new RuntimeException("Class must be contained in table.");
        }
        
        return table.get(className).indexOf(methodName);
    }
    
    public List<Pair> get(Identifier className)
    {
        return table.get(className);
    }
    
    public String toString()
    {
        StringBuilder result = new StringBuilder();
        
        for (Identifier className : table.keySet())
        {
            result.append(className);
            result.append(": ");
            result.append(table.get(className));
            result.append("\n");
        }
        
        
        return result.toString();
    }
}

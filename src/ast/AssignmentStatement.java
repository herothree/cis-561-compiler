package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class AssignmentStatement extends Statement {
	private Lexpr assignee;
	private Rexpr toAssign;
	protected Identifier type;
	
	public AssignmentStatement(Lexpr assignee, Identifier type , Rexpr toAssign, Location l)
	{
		super(l);
		this.setAssignee(assignee);
		this.setToAssign(toAssign);
		this.type = type;
	}

	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>(
				Arrays.asList(new QuackAcceptor[]{getAssignee(), type, getToAssign()}));
	}

	public Lexpr getAssignee() {
		return assignee;
	}

	public void setAssignee(Lexpr assignee) {
		this.assignee = assignee;
	}
	
	public Identifier getType()
	{
		return this.type;
	}

	public Rexpr getToAssign() {
		return toAssign;
	}

	public void setToAssign(Rexpr toAssign) {
		this.toAssign = toAssign;
	}
}

package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class FieldLexpr extends Lexpr {

	private Identifier fieldName;
	private Rexpr className;
	
	public FieldLexpr(Rexpr className, String fieldName, Location l)
	{
		super(l);
		this.setFieldName(new Identifier(fieldName, l));
		this.setClassName(className);
	}
	
	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>(
				Arrays.asList(new QuackAcceptor[]{getFieldName(), getClassName()}));
	}

	public Rexpr getClassName() {
		return className;
	}

	public void setClassName(Rexpr className) {
		this.className = className;
	}

	public Identifier getFieldName() {
		return fieldName;
	}

	public void setFieldName(Identifier fieldName) {
		this.fieldName = fieldName;
	}
	
}

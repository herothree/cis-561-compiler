package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class WhileStatement extends Statement {

	protected Rexpr condition;
	protected StatementBlock statementBlock;
	
	public WhileStatement(Rexpr condition, StatementBlock statementBlock, Location l)
	{
		super(l);
		this.condition = condition;
		this.statementBlock = statementBlock;
	}
	
	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>(
				Arrays.asList(new AbstractSyntaxTreeNode[]{condition, statementBlock}));
	}

	public Rexpr getCondition() {
		return condition;
	}

	public void setCondition(Rexpr condition) {
		this.condition = condition;
	}

	public StatementBlock getStatementBlock() {
		return statementBlock;
	}

	public void setStatementBlock(StatementBlock statementBlock) {
		this.statementBlock = statementBlock;
	}
}

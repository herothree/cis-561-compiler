package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class ElseBlock extends Statement
{
	protected StatementBlock statementBlock;
	
	public ElseBlock(StatementBlock statementBlock, Location l)
	{
		super(l);
		this.statementBlock = statementBlock;
	}
	
	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>(
				Arrays.asList(new QuackAcceptor[]{statementBlock}));
	}

	public StatementBlock getStatementBlock() {
		return statementBlock;
	}

	public void setStatementBlock(StatementBlock statementBlock) {
		this.statementBlock = statementBlock;
	}
}

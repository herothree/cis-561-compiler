package ast;

import java.util.*;

import codegen.CodeGenerator;
import compiler.QuackAcceptor;
import compiler.QuackVisitor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class Program extends AbstractSyntaxTreeNode
{
    protected ASTNodeList<ClassDef> classList;
    protected ClassDef mainClass;

    public Program(ASTNodeList<ClassDef> cl, ASTNodeList<Statement> mainStatements, Location l)
    {
        super(l);
        this.classList = cl;
        
        // It's easier to generate code if the statements at the end are part of a method,
        // rather than just being free statements. So rig up a class to hold them here.
        this.mainClass = new ClassDef(
                new ClassSignature(CodeGenerator.MAIN_CLASSNAME, new ASTNodeList<FormalArg>(), "Obj", l),
                new ClassBody(mainStatements, new ASTNodeList<MethodDef>(), null), null);
    }

    /**
     * Performs a depth-first post-order traversal of the tree.
     */
    public void accept(QuackVisitor v)
    {
        if (!v.preVisit(this))
        {
            return;
        }

        for (QuackAcceptor f : new QuackAcceptor[] { classList, mainClass })
        {
            try
            {
                f.accept(v);
            }
            catch (IllegalArgumentException ex)
            {

            }
        }

        v.visit(this);

        v.postVisit(this);
    }

    public List<QuackAcceptor> getChildren()
    {
        List<QuackAcceptor> result = new ArrayList<QuackAcceptor>();
        result.addAll(classList);
        result.add(mainClass);
        return result;
    }

    public ASTNodeList<ClassDef> getClassList()
    {
        return classList;
    }

    public void setClassList(ASTNodeList<ClassDef> classList)
    {
        this.classList = classList;
    }

    public ClassDef getMainClass()
    {
        return mainClass;
    }

    public void setStmtList(ClassDef mainClass)
    {
        this.mainClass = mainClass;
    }
}

package ast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import compiler.QuackAcceptor;
import compiler.QuackVisitor;
import java_cup.runtime.ComplexSymbolFactory.Location;

/**
 * 
 * This class is essentially an ArrayList but it has better pretty printing and methods
 * for iterating over its contents and performing AST actions on them.
 * @param <E> The Type parameter
 */
public class ASTNodeList<E extends AbstractSyntaxTreeNode> extends AbstractSyntaxTreeNode implements List<E> {

	protected List<E> contents;
	
	public ASTNodeList(Location l)
	{
		super(l);
		contents = new ArrayList<E>();
	}
	
	public ASTNodeList()
	{
		this(null);
	}
	
	public void accept(QuackVisitor v)
	{
		if (!v.preVisit(this))
		{
			return;
		}
		
		for (AbstractSyntaxTreeNode astn : contents)
		{
			astn.accept(v);
		}

		v.visit(this);
		
		v.postVisit(this);
	}
	
	@Override
	public int size() {
		return contents.size();
	}

	@Override
	public boolean isEmpty() {
		return contents.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return contents.contains(o);
	}

	@Override
	public Iterator<E> iterator() {
		return contents.iterator();
	}

	@Override
	public Object[] toArray() {
		return contents.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return contents.toArray(a);
	}

	@Override
	public boolean add(E e) {
		return contents.add(e);
	}

	@Override
	public boolean remove(Object o) {
		return contents.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return contents.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		return contents.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		return contents.addAll(index, c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return contents.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return contents.retainAll(c);
	}

	@Override
	public void clear() {
		contents.clear();
	}

	@Override
	public E get(int index) {
		return contents.get(index);
	}

	@Override
	public E set(int index, E element) {
		return contents.set(index, element);
	}

	@Override
	public void add(int index, E element) {
		contents.add(index, element);
	}

	@Override
	public E remove(int index) {
		return contents.remove(index);
	}

	@Override
	public int indexOf(Object o) {
		return contents.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		return contents.lastIndexOf(o);
	}

	@Override
	public ListIterator<E> listIterator() {
		return contents.listIterator();
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		return contents.listIterator(index);
	}

	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		return contents.subList(fromIndex, toIndex);
	}

	@Override
	public List<QuackAcceptor> getChildren() {
		List<QuackAcceptor> result = new ArrayList<QuackAcceptor>();
		result.addAll(this);
		return result;
	}


}

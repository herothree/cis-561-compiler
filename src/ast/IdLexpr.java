package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class IdLexpr extends Lexpr {
	
	private Identifier ident;
	
	public IdLexpr(String ident, Location l)
	{
		this(new Identifier(ident), l);
	}
	
	public IdLexpr(Identifier id, Location l)
	{
		super(l);
		this.setIdent(id);
	}
	
	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>(
				Arrays.asList(new QuackAcceptor[]{getIdent()}));
	}
	
	public String toString()
	{
		return getIdent().getData();
	}

	public Identifier getIdent() {
		return ident;
	}

	public void setIdent(Identifier ident) {
		this.ident = ident;
	}
	
}

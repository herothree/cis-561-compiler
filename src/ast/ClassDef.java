
package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class ClassDef extends AbstractSyntaxTreeNode
{
	private ClassSignature classSignature;
	protected ClassBody classBody;
	protected int scope;
	
	public ClassDef(ClassSignature classSignature, ClassBody classBody, Location l)
	{
		super(l);
		this.setClassSignature(classSignature);
		this.classBody = classBody;
		this.scope = -1;
	}
	
	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>(
				Arrays.asList(new QuackAcceptor[]{getClassSignature(), classBody}));
	}

	public ClassSignature getClassSignature() {
		return classSignature;
	}

	public void setClassSignature(ClassSignature classSignature) {
		this.classSignature = classSignature;
	}

    public ClassBody getClassBody()
    {
        return classBody;
    }

    public void setClassBody(ClassBody classBody)
    {
        this.classBody = classBody;
    }

    public int getScope()
    {
        return scope;
    }

    public void setScope(Integer scope)
    {
        this.scope = scope;
    }
}

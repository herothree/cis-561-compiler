package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class IfStatement extends Statement {
	
	protected Rexpr condition;
	protected StatementBlock stmts;
	protected List<ElifBlock> elifBlocks;
	protected ElseBlock elseBlock;
	
	public IfStatement(Rexpr condition, StatementBlock stmts, List<ElifBlock> elifBlocks, ElseBlock elseBlock, Location l)
	{
		super(l);
		
		this.condition = condition;
		this.stmts = stmts;
		this.elifBlocks = elifBlocks;
		this.elseBlock = elseBlock;
	}

	public List<QuackAcceptor> getChildren()
	{
		ArrayList<QuackAcceptor> result = new ArrayList<QuackAcceptor>(
				Arrays.asList(new QuackAcceptor[]{condition, stmts}));
		result.addAll(elifBlocks);
		result.add(elseBlock);
		return result;
	}

	public Rexpr getCondition() {
		return condition;
	}

	public void setCondition(Rexpr condition) {
		this.condition = condition;
	}

	public StatementBlock getStmts() {
		return stmts;
	}

	public void setStmts(StatementBlock stmts) {
		this.stmts = stmts;
	}

	public List<ElifBlock> getElifBlocks() {
		return elifBlocks;
	}

	public void setElifBlock(List<ElifBlock> elifBlocks) {
		this.elifBlocks = elifBlocks;
	}

	public ElseBlock getElseBlock() {
		return elseBlock;
	}

	public void setElseBlock(ElseBlock elseBlock) {
		this.elseBlock = elseBlock;
	}
}

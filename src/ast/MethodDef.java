package ast;

import java.util.*;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class MethodDef extends AbstractSyntaxTreeNode
{
	protected Identifier methodName;
	protected List<FormalArg> formalArgs;
	protected Identifier returnType;
	protected StatementBlock stmts;
	protected int scope;
	
	public MethodDef(String methodName, List<FormalArg> formalArgs, Identifier returnType, StatementBlock stmts, Location l)
	{
		super(l);
		this.methodName = new Identifier(methodName, l);
		this.formalArgs = formalArgs;
		this.returnType = returnType;
		this.stmts = stmts;
		this.scope = -1;
	}
	
	public List<QuackAcceptor> getChildren()
	{
		List<QuackAcceptor> result = new ArrayList<QuackAcceptor>();
		result.add(methodName);
		result.addAll(formalArgs);
		result.add(returnType);
		result.add(stmts);
		return result;
	}

	public Identifier getMethodName() {
		return methodName;
	}

	public void setMethodName(Identifier methodName) {
		this.methodName = methodName;
	}

	public List<FormalArg> getFormalArgs() {
		return formalArgs;
	}

	public void setFormalArgs(List<FormalArg> formalArgs) {
		this.formalArgs = formalArgs;
	}

	public Identifier getReturnType() {
		return returnType;
	}

	public void setReturnType(Identifier returnType) {
		this.returnType = returnType;
	}

	public StatementBlock getStatements() {
		return stmts;
	}

	public void setStatements(StatementBlock stmts) {
		this.stmts = stmts;
	}

    public int getScope()
    {
        return scope;
    }

    public void setScope(int scope)
    {
        this.scope = scope;
    }
	
	
}

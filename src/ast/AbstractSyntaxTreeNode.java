package ast;


import compiler.QuackAcceptor;
import compiler.QuackVisitor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public abstract class AbstractSyntaxTreeNode implements QuackAcceptor {

	private Location location;
	
	public AbstractSyntaxTreeNode(Location l)
	{
		this.setLocation(l);
	}
	
	public String toString()
	{
		return "" + this.getClass();
		
	}
	
	/**
	 * Performs a depth-first post-order traversal of the tree.
	 */
	public void accept(QuackVisitor v)
	{
		if (!v.preVisit(this))
		{
			return;
		}
		
		for (QuackAcceptor qa : this.getChildren())
		{
			if (null != qa)
			{
				qa.accept(v);
			}
		}
		
		v.visit(this);
		
		v.postVisit(this);
		
		return;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		return false;
	}
	
	
	
}

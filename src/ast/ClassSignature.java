package ast;

import java.util.*;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class ClassSignature extends AbstractSyntaxTreeNode
{
	private Identifier className;
	protected List<FormalArg> formalArgs;
	private Identifier parentClassName;
	
	public List<FormalArg> getFormalArgs() {
		return formalArgs;
	}

	public void setFormalArgs(List<FormalArg> formalArgs) {
		this.formalArgs = formalArgs;
	}

	public ClassSignature(String className, List<FormalArg> formalArgs,
			String parentClassName, Location l)
	{
		super(l);
		this.setClassName(new Identifier(className, l));
		this.formalArgs = formalArgs;
		this.setParentClassName(new Identifier(parentClassName, l));
	}
	
	public List<QuackAcceptor> getChildren()
	{
		List<QuackAcceptor> result = new ArrayList<QuackAcceptor>();
		result.add(getClassName());
		result.addAll(formalArgs);
		result.add(getParentClassName());
		return result;
	}

	public Identifier getClassName() {
		return className;
	}

	public void setClassName(Identifier className) {
		this.className = className;
	}

	public Identifier getParentClassName() {
		return parentClassName;
	}

	public void setParentClassName(Identifier parentClassName) {
		this.parentClassName = parentClassName;
	}
}

package ast;

import java.util.*;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class ClassBody extends AbstractSyntaxTreeNode
{
	protected List<Statement> stmts;
	protected List<MethodDef> methods;
	
	public ClassBody(List<Statement> stmts, List<MethodDef> methods, Location l)
	{
		super(l);
		this.stmts = stmts;
		this.methods = methods;
	}
	
	public List<QuackAcceptor> getChildren()
	{
		List<QuackAcceptor> result = new ArrayList<QuackAcceptor>();
		result.addAll(stmts);
		result.addAll(methods);
		return result;
	}
}

package ast;

import java.util.ArrayList;
import java.util.List;

import compiler.QuackAcceptor;
import compiler.QuackVisitor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class IntConst extends Rexpr {

	protected int data;
	
	public IntConst(int data, Location l) 
	{
		super(l);
		this.data = data;
	}

	public void accept(QuackVisitor v)
	{
		if (!v.preVisit(this))
		{
			return;
		}
		
		v.visit(this);
		
		v.postVisit(this);
	}
	
	public String toString()
	{
		return Integer.toString(this.data);
	}
	
	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>();
	}

    public int getData()
    {
        return data;
    }

    public void setData(int data)
    {
        this.data = data;
    }
	
}

package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class ExprStatement extends Statement 
{
	protected Rexpr toExecute;
	
	public ExprStatement(Rexpr toExecute, Location l)
	{
		super(l);
		this.toExecute = toExecute;
	}
	
	public Rexpr getToExecute() {
		return toExecute;
	}

	public void setToExecute(Rexpr toExecute) {
		this.toExecute = toExecute;
	}

	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>(
				Arrays.asList(new QuackAcceptor[]{toExecute}));
	}
}

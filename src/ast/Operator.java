package ast;

import java.util.ArrayList;
import java.util.List;

import compiler.QuackAcceptor;
import compiler.QuackVisitor;

public enum Operator implements QuackAcceptor {
	TIMES,
	AND,
	ATLEAST,
	IDENT,
	SEMICOLON,
	CLASS,
	ELSE,
	PLUS,
	INT,
	RPAREN,
	RBRACE,
	OR,
	WHILE,
	NOT,
	RETURN,
	IF,
	COLON,
	LPAREN,
	LBRACE,
	STRING,
	EQUALS,
	COMMA,
	GETS,
	EOF,
	DIVIDE,
	MORE,
	DEF,
	ELIF,
	MINUS,
	DOT,
	ATMOST,
	EXTENDS,
	LESS,
	error;

	@Override
	public void accept(QuackVisitor v)
	{
		v.visit(this);
	}

	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>();
	}

}

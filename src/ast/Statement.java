package ast;

import java.util.ArrayList;
import java.util.List;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class Statement extends AbstractSyntaxTreeNode
{
	public Statement(Location l)
	{
		super(l);
	}
	
	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>();
	}
}

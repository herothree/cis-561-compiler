package ast;

import java.util.*;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class MethodCall extends Rexpr {

	public MethodCall(Rexpr callingInstance, String methodName, List<Rexpr> actualArgs, Location l) 
	{
		super(l);
		this.callingInstance = callingInstance;
		this.methodName = new Identifier(methodName, l);
		this.actualArgs = actualArgs;
	}
	
	public Rexpr getCallingInstance() {
		return callingInstance;
	}

	public void setCallingInstance(Rexpr callingInstance) {
		this.callingInstance = callingInstance;
	}

	public Identifier getMethodName() {
		return methodName;
	}

	public void setMethodName(Identifier methodName) {
		this.methodName = methodName;
	}

	public List<Rexpr> getActualArgs() {
		return actualArgs;
	}

	public void setActualArgs(List<Rexpr> actualArgs) {
		this.actualArgs = actualArgs;
	}

	protected Rexpr callingInstance;
	protected Identifier methodName;
	protected List<Rexpr> actualArgs;
	
	public List<QuackAcceptor> getChildren()
	{
		List<QuackAcceptor> result = new ArrayList<QuackAcceptor>();
		result.add(callingInstance);
		result.add(methodName);
		result.addAll(actualArgs);
		return result;
	}

}


package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class Unop extends Rexpr {

	protected Operator op;
	protected Rexpr r1;
	
	public Unop(Rexpr r1, Operator op, Location l) 
	{
		super(l);
		this.op = op;
		this.r1 = r1;
	}
	
	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>(
				Arrays.asList(new QuackAcceptor[]{op, r1}));
	}

	public Operator getOp() {
		return op;
	}

	public void setOp(Operator op) {
		this.op = op;
	}

	public Rexpr getR1() {
		return r1;
	}

	public void setR1(Rexpr r1) {
		this.r1 = r1;
	}
}

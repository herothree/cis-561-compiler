package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class ReturnStatement extends Statement
{
	protected Rexpr result;
	
	public Rexpr getResult() {
		return result;
	}

	public void setResult(Rexpr result) {
		this.result = result;
	}

	public ReturnStatement(Rexpr result, Location l) throws Exception
	{
		super(l);
		
		if (null == result)
		{
			throw new Exception("ReturnStatement shouldn't ever have null");
		}
		
		this.result = result;
	}
	
	public ReturnStatement(Location l) throws Exception
	{
		this(new IdLexpr("Nothing", l), l);
	}
	
	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>(
				Arrays.asList(new QuackAcceptor[]{result}));
	}
}

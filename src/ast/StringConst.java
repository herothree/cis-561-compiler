package ast;

import java.util.*;

import compiler.QuackAcceptor;
import compiler.QuackVisitor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class StringConst extends Rexpr {
	
	protected String data;
	
	public StringConst(String str, Location l) 
	{
		super(l);
		data = str;
	}
	
	public void accept(QuackVisitor v)
	{
		if (!v.preVisit(this))
		{
			return;
		}
		
		v.visit(this);
		
		v.postVisit(this);
	}
	
	public String toString()
	{
		return this.data;
	}
	
	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>();
	}

    public String getData()
    {
        return data;
    }

    public void setData(String data)
    {
        this.data = data;
    }
}

package ast;

import java.util.ArrayList;
import java.util.List;

import compiler.QuackAcceptor;
import compiler.QuackVisitor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class BoolConst extends Rexpr {

	protected boolean data;
	
	public BoolConst(boolean data, Location l) 
	{
		super(l);
		this.data = data;
	}
	
	public boolean getData() {
		return data;
	}

	public void setData(boolean data) {
		this.data = data;
	}

	public void accept(QuackVisitor v)
	{
		if (!v.preVisit(this))
		{
			return;
		}
		
		v.visit(this);
		
		v.postVisit(this);
	}
	
	public String toString()
	{
		return Boolean.toString(this.data);
	}
	
	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>();
	}
	
}

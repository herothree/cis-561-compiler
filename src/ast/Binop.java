package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import compiler.QuackAcceptor;
import compiler.QuackVisitor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class Binop extends Rexpr {

	public Rexpr getR1() {
		return r1;
	}

	public void setR1(Rexpr r1) {
		this.r1 = r1;
	}

	public Rexpr getR2() {
		return r2;
	}

	public void setR2(Rexpr r2) {
		this.r2 = r2;
	}

	public Operator getOp() {
		return op;
	}

	public void setOp(Operator op) {
		this.op = op;
	}

	protected Rexpr r1;
	protected Rexpr r2;
	protected Operator op;
	
	/**
	 * 
	 * @param r1
	 * @param r2
	 * @param op Should be a constant from the parser.sym class
	 */
	public Binop(Rexpr r1, Rexpr r2, Operator op, Location l) 
	{
		super(l);
		this.r1 = r1;
		this.r2 = r2;
		this.op = op;
	}
	
	public void accept(QuackVisitor v)
	{
		if (!v.preVisit(this))
		{
			return;
		}
		
		v.visit(this);
		
		r1.accept(v);
		op.accept(v);
		r2.accept(v);
		
		v.postVisit(this);
	}
	
	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>(
				Arrays.asList(new QuackAcceptor[]{r1, op, r2}));
	}
	
}

package ast;

import java.util.*;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class ConstructorCall extends Rexpr {

	private Identifier className;
	protected List<Rexpr> actualArgs;
	
	public List<Rexpr> getActualArgs() {
		return actualArgs;
	}

	public void setActualArgs(List<Rexpr> actualArgs) {
		this.actualArgs = actualArgs;
	}
	
	public ConstructorCall(String methodName, List<Rexpr> actualArgs, Location l)
	{
		super(l);
		this.setClassName(new Identifier(methodName, l));
		this.actualArgs = actualArgs;
	}
	
	public List<QuackAcceptor> getChildren()
	{
		List<QuackAcceptor> result = new ArrayList<QuackAcceptor>();
		result.add(getClassName());
		result.addAll(actualArgs);
		return result;
	}

	public Identifier getClassName() {
		return className;
	}

	public void setClassName(Identifier className) {
		this.className = className;
	}

}



package ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class FormalArg extends AbstractSyntaxTreeNode
{
	protected Identifier argName;
	protected Identifier argType;

	public FormalArg(String argName, String argType, Location l)
	{
		super(l);
		this.argName = new Identifier(argName, l);
		this.argType = new Identifier(argType, l);
	}
	
	   public FormalArg(String argName, String argType)
	   {
	        this(argName, argType, null);
	   }
	
	public String toString()
	{
		return String.format("%s: %s", argName.getData(), argType.getData());
	}
	
	public Identifier getArgName() {
		return argName;
	}

	public void setArgName(Identifier argName) {
		this.argName = argName;
	}

	public Identifier getArgType() {
		return argType;
	}

	public void setArgType(Identifier argType) {
		this.argType = argType;
	}

	

	
	public List<QuackAcceptor> getChildren()
	{
		return new ArrayList<QuackAcceptor>(
				Arrays.asList(new QuackAcceptor[]{argName, argType}));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 593;
		result = prime * result + ((argName == null) ? 0 : argName.hashCode());
		result = prime * result + ((argType == null) ? 0 : argType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		FormalArg other = (FormalArg) obj;
		if (argName == null) {
			if (other.argName != null)
				return false;
		} else if (!argName.equals(other.argName))
			return false;
		if (argType == null) {
			if (other.argType != null)
				return false;
		} else if (!argType.equals(other.argType))
			return false;
		return true;
	}
}

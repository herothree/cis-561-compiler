package ast;

import java.util.*;

import compiler.QuackAcceptor;
import java_cup.runtime.ComplexSymbolFactory.Location;

public class StatementBlock extends AbstractSyntaxTreeNode
{
	protected List<Statement> statements;
	
	public StatementBlock(List<Statement> statements, Location l)
	{
		super(l);
		this.statements = statements;
	}

	@Override
	public List<QuackAcceptor> getChildren() {
		List<QuackAcceptor> result = new ArrayList<QuackAcceptor>();
		result.addAll(statements);
		return result;
	}
}

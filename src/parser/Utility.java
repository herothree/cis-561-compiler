package parser;
class Utility {

    private static final String errorMsg[] = {
        "Error: Unmatched end-of-comment punctuation.",
        "Error: Unmatched start-of-comment punctuation.",
        "Error: Unclosed string.",
        "Error: Illegal character."
    };

    public static final int E_ENDCOMMENT = 0; 
    public static final int E_STARTCOMMENT = 1; 
    public static final int E_UNCLOSEDSTR = 2; 
    public static final int E_UNMATCHED = 3; 

    public static void error(int code) {
        System.err.println(errorMsg[code]);
    }

    public static void error(int code, int line, String text) {
        if (E_UNCLOSEDSTR == code) {
            System.err.println(line + ":Unclosed string?  Encountered newline in quoted string."
                    + "(at '" + text + "')");
        }
        else if (E_UNMATCHED == code) {
            System.err.printf("%d: Illegal character: %s \n", line, text);
        }
        else {
            error(code);
        }
    }
}


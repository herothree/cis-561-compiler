#!/usr/bin/env python3
#
# Jeremy Sigrist
# This program generates a driver script for the java parser.
# Using a script to generate a script ensures that the paths are always accurate 
# and machine independent.

import os
from sys import argv

def write_driver(base_path, stream):
    print('#!/bin/bash', file = stream)
    print('if [ $# -eq 0 ]; then', file = stream)
    print('    java -cp "' + os.path.dirname(base_path) + ':', file = stream, end = '')
    print("{}".format(os.path.dirname(os.path.dirname(base_path))) + '/java-cup-0.11a.jar" ' + '"parser.QuackParser" < /dev/stdin', file = stream)
    print('else', file = stream)
    print('    java -cp "' + os.path.dirname(base_path) + ':', file = stream, end = '')
    print("{}".format(os.path.dirname(os.path.dirname(base_path))) + '/java-cup-0.11a.jar" ' + '"parser.QuackParser" $@', file = stream)
    print('fi', file = stream)


if __name__ == '__main__':

    # Make sure we have enough command line arguments
    if len(argv) != 2:
        print('Usage: ./driver_gen [filename]')
        exit(1)

    with open(argv[1], mode='w') as input_file:
        write_driver(os.getcwd(), input_file)

    os.chmod(argv[1], 0o755)


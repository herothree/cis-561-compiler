package errors;

import java_cup.runtime.ComplexSymbolFactory.Location;

public class NonexistentMethodException extends CompilerException {

	public NonexistentMethodException(String string, Location loc) {
		super(string, loc);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8598177412598879745L;

}

package errors;

import java_cup.runtime.ComplexSymbolFactory.Location;

public class MismatchingOverrideException extends CompilerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4873246369112975412L;

	public MismatchingOverrideException(String msg, Location loc) {
		super(msg, loc);
		// TODO Auto-generated constructor stub
	}

}

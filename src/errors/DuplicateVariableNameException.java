package errors;

import java_cup.runtime.ComplexSymbolFactory.Location;

public class DuplicateVariableNameException extends CompilerException {

	public DuplicateVariableNameException(String string, Location loc) {
		super(string, loc);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5135683471152040859L;

}

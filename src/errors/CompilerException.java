package errors;

import java_cup.runtime.ComplexSymbolFactory.Location;

public class CompilerException extends RuntimeException {

	protected Location l;
	
	protected String text;
	protected Integer line;
	protected Integer column;
	
	
	public CompilerException(String msg, Location loc)
	{
		super(msg);
		this.l = loc;
		
		this.text = msg;
		
		if (null != loc)
		{
	        this.line = loc.getLine();
	        this.column = loc.getColumn();
		}
		else
		{
		    this.line = -1;
		    this.column = -1;
		}

		
	}

	public Location getLocation() {
		return l;
	}

	public void setLocation(Location loc) {
		this.l = loc;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3106939958351786722L;


    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((column == null) ? 0 : column.hashCode());
        result = prime * result + ((line == null) ? 0 : line.hashCode());
        result = prime * result + ((text == null) ? 0 : text.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CompilerException other = (CompilerException) obj;
        if (column == null)
        {
            if (other.column != null)
                return false;
        }
        else if (!column.equals(other.column))
            return false;
        if (line == null)
        {
            if (other.line != null)
                return false;
        }
        else if (!line.equals(other.line))
            return false;
        if (text == null)
        {
            if (other.text != null)
                return false;
        }
        else if (!text.equals(other.text))
            return false;
        return true;
    }

}

package errors;

import java_cup.runtime.ComplexSymbolFactory.Location;

public class IncorrectArgumentsException extends CompilerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3681220423404825113L;

	public IncorrectArgumentsException(String string, Location loc) {
		super(string, loc);
		// TODO Auto-generated constructor stub
	}

}

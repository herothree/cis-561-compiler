package errors;

import java_cup.runtime.ComplexSymbolFactory.Location;

public class TypeErrorException extends CompilerException {

	public TypeErrorException(String string, Location loc) {
		super(string, loc);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4348467422913210923L;

}

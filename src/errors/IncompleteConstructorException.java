package errors;

import java_cup.runtime.ComplexSymbolFactory.Location;

public class IncompleteConstructorException extends CompilerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2948861557927659883L;

	public IncompleteConstructorException(String msg, Location loc) {
		super(msg, loc);
		// TODO Auto-generated constructor stub
	}

}

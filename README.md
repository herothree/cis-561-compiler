### A compiler for the CIS 561 course at the University of Oregon, Winter 2017 ###

This project implements a compiler for the quack educational language developed by [Dr. Michal Young](http://ix.cs.uoregon.edu/~michal/) at the University of Oregon. It implements variable assignments, conditional statements, loop statements, object oriented concepts including inheritance and dynamic dispatch, and recursion. 

Thank you to Andrew Hampton for some of the test files:  
    class.qk  
    math.qk  
    operators.qk  
    while.qk
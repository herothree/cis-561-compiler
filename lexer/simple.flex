/* this is the scanner example from the JLex website 
   (with small modifications to make it more readable) */

/* Code above this line is copied into the top of the generated file */
%%

%unicode

/* Copied into src of generated class. Can define vars and methods here */ 
%{
  private int comment_count = 0;
%} 

/* Turn on line, column, and character counting (accessed via yyline, yycolumn, yychar variables) */
%line
%column
%char

%state COMMENT
%full

/* Use this option to make a release build */
%standalone 

/* This option prints information about every match to standard out */
/* %debug */


ALPHA=[A-Za-z]
DIGIT=[0-9]
NONNEWLINE_WHITE_SPACE_CHAR=[\ \t]
NEWLINE=\r|\n|\r\n
WHITE_SPACE_CHAR=[\n\r\ \t]
STRING_TEXT=((\\\")|(\\0)|(\\b)|(\\t)|(\\n)|(\\r)|(\\f)|(\\\\)|[^\n\r\"\\])*
COMMENT_TEXT=([^*/\n]|[^*\n]"/"[^*\n]|[^/\n]"*"[^/\n]|"*"[^/\n]|"*"[\r\n]|"/"[^*\n])*

/* MULTILINE_STRING_TEXT=([^/\"{3}]|(\r\n)|\r|\n)* */
MULTILINE_STRING_TEXT=(\"[^\"]|\"\"[^\"]|[^\"])*



%% 

/*TODO: CHECK LENGTHS OF ALL OF THESE*/

<YYINITIAL> {

  /* Match single line comments */
  "//".* { }

  /* Match keywords */
  "class" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+5, "CLASS")); }
  "def" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+3, "DEF")); }
  "extends" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+7, "EXTENDS")); }
  "if" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+2, "IF")); }
  "elif" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+4, "ELIF")); }
  "else" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+4, "ELSE")); }
  "while" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+5, "WHILE")); }
  "return" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+6, "RETURN")); }
  
  /* Don't think I need to handle predefined identifiers, but if I do it goes here */

  /* Match punctuation */
  "+" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "PLUS")); }
  "-" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "MINUS")); }
  "*" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "TIMES")); }
  "/" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "DIVIDE")); }
  "==" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+2, "EQUALS")); }
  "<=" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+2, "ATMOST")); }
  ">=" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+2, "ATLEAST")); }
  "<" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "LESS")); }
  ">" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "MORE")); }
  "and" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+3, "AND")); }
  "or" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+2, "OR")); }
  "not" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+3, "NOT")); }
  "{" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "LBRACE")); }
  "}" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "RBRACE")); }
  "=" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "GETS")); }
  "(" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "LPAREN")); }
  ")" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "RPAREN")); }
  "," { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "COMMA")); }
  ";" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "SEMICOLON")); }
  "." { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "DOT")); }
  ":" { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "COLON")); }

  /* Match identifiers */
  [a-zA-Z_][a-zA-Z0-9_]* { return (new Yytoken(0,yytext(),yyline,yychar,yychar+1, "IDENT")); }

  /* Match integer literals */
  [0-9]+ { return (new Yytoken(0,yytext(),yyline,yychar,yychar+yytext().length(), "INT")); }

  {NONNEWLINE_WHITE_SPACE_CHAR}+ { }

  "/*" { yybegin(COMMENT); comment_count++; }

  
  "\"\"\""{MULTILINE_STRING_TEXT}"\"\"\"" {
    String str = yytext().substring(3, yytext().length()-3);
    return (new Yytoken(40,str,yyline,yychar,yychar+yylength(), "MULTILINE_STRING_LIT"));
  }

  \"{STRING_TEXT}\" {
    String str =  yytext().substring(0,yytext().length());
    return (new Yytoken(40,str,yyline,yychar,yychar+yylength(), "STRING_LIT"));
  }
  
  \"{STRING_TEXT}(\r\n|[\r\n]) {
    String str =  yytext().substring(0,yytext().length());
    Utility.error(Utility.E_UNCLOSEDSTR, yyline, yytext());
    return (new Yytoken(41,str,yyline,yychar,yychar + str.length(), "ERROR"));
  }
  
  /* {DIGIT}+ { return (new Yytoken(42,yytext(),yyline,yychar,yychar+yylength())); } */
}

<COMMENT> {
  "/*" { comment_count++; }
  "*/" { if (--comment_count == 0) yybegin(YYINITIAL); }
  {COMMENT_TEXT} { }
}


{NEWLINE} { }

. {
    Utility.error(Utility.E_UNMATCHED, yyline, yytext());
}


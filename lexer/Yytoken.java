class Yytoken {
    public int m_index;
    public String m_text;
    public int m_line;
    public int m_charBegin;
    public int m_charEnd;
    public String m_category;

    Yytoken (int index, String text, int line, int charBegin, int charEnd, String category) {
        m_index = index;
        m_text = text;
        m_line = line;
        m_charBegin = charBegin;
        m_charEnd = charEnd;
        m_category = category;
    }

    Yytoken (int index, String text, int line, int charBegin, int charEnd)
    {
        this(index, text, line, charBegin, charEnd, "UNKNOWN");
    }

    public String toString() {
        //return "Text   : "+m_text+ "\nindex : "+m_index+ "\nline  : "+m_line+ "\ncBeg. : "+m_charBegin+ "\ncEnd. : "+m_charEnd;
        //System.err.println((m_line + 1) + " " + m_category + " " + m_text + "");
        //System.err.println((m_line + 1) + " " + m_category + " \"" + m_text + "\"");
        return (m_line + 1) + " " + m_category + " \"" + m_text + "\"";
    }
}


Quack compiler for CIS 561
Jeremy Sigrist

Working
    Arithemetic
    String manipulation
    type checking (I think?)
    recursion
    inheritance
    member variables
    dynamic dispatch
    short curcuiting evaluation
    if/elif/else statements
    while statements
    returning values
    

Not working
    Ensuring that all paths return a value in a method with a return value
    Error messages for lexing and parsing could be prettier. 
        Type checker ones should be useable though
    There's a precedence bug involving not and ==. 
        while (not a == b) // doesn't always work as desired
        while (not (a == b)) // seems to work


Notes:
    Compiles to C, then calls gcc to compile that file into native code
    Output binary is named "output" and can be found in the same folder that the compiler call was made in
    Thank you to Andrew Hampton for some of the test files
        class.qk
        math.qk
        operators.qk
        while.qk


